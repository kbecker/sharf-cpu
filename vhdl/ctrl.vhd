--Copyright (C) 2013 Cem Bassoy.
--
--This file is part of the Sharf CPU.
--
--Sharf CPU is free software: you can redistribute it and/or modify
--it under the terms of the GNU General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.
--
--Sharf CPU is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU General Public License for more details.
--
--You should have received a copy of the GNU General Public License
--along with Sharf CPU.  If not, see <http:--www.gnu.org/licenses/>.


-- In Register i0 wird die Anzahl der Iterationen für die hardwareschleife ijnz gespeichert.
-- In Register i15 wird die Rücksprungadresse für den funktionsaufruf call gespeichert.
-- In Register i14 wird der Stackpointer für den imem gespeichert.
-- In Register i13 wird der Stackpointer für den dmem gespeichert.
-- wird ein indexregister erhöht, dann müssen inop hinzugefügt werden.
-- Nach einem Call kann ein rts, inop danach. // i15 hält das rts
-- cond jmp braucht inop.

--  jmp a             : 00 00 00 aaaa aaaa aaaa : 0x00aaa                         
--  cjf a             : 00 00 01 aaaa aaaa aaaa : 0x01aaa
--  call a            : 00 00 10 aaaa aaaa aaaa : 0x02aaa
--  ijnz a            : 00 00 11 aaaa aaaa aaaa : 0x03aaa

--  jmp is            : 00 01 0* **** 0001 ssss : 0x04*1s 
--  cjf is            : 00 01 0* **** 0010 ssss : 0x04*2s
--  call is           : 00 01 0* **** 0100 ssss : 0x04*4s
--  ijnz is           : 00 01 0* **** 1000 ssss : 0x04*8s

--  ir = io(a)        : 00 01 1* rrrr 0001 aaaa : 0x06r1a
--  dr = io(a)        : 00 01 1* rrrr 0010 aaaa : 0x06r2a
--  io(a) = is        : 00 01 1* aaaa 0100 ssss : 0x06a4s
--  io(a) = ds        : 00 01 1* aaaa 1000 ssss : 0x06a8s

--  f  = it > is      : 00 10 00 0001 tttt ssss : 0x081ts
--  f  = it <= is     : 00 10 00 0010 tttt ssss : 0x082ts
--  f  = it == is     : 00 10 00 0100 tttt ssss : 0x084ts
--  f  = it != is     : 00 10 00 1000 tttt ssss : 0x088ts

--  ir = it&is        : 00 10 01 rrrr tttt ssss : 0x09rts
--  ir = it|is        : 00 10 10 rrrr tttt ssss : 0x0Arts
--  ir = is>>1        : 00 10 11 rrrr ***0 ssss : 0x0Br0s
--  ir = ~is          : 00 10 11 rrrr ***1 ssss : 0x0Br1s 

--  ir = it*is        : 00 11 00 rrrr tttt ssss : 0x0Crts
--  ir = it+is        : 00 11 01 rrrr tttt ssss : 0x0Drts
--  ir = it-is        : 00 11 10 rrrr tttt ssss : 0x0Erts
--  ir = is           : 00 11 11 rrrr **** ssss : 0x0Fr*s

--  ir.l = n          : 01 00 0n rrrr nnnn nnnn : 0x10rnn
--  ir.h = n          : 01 00 1n rrrr nnnn nnnn : 0x12rnn

--  ir += n           : 01 01 0n rrrr nnnn nnnn : 0x14rnn 
--  ir  = n           : 01 01 1n rrrr nnnn nnnn : 0x16rnn

--  ir=imem(a)        : 01 10 00 rrrr aaaa aaaa : 0x18raa
--  dr=dmem(a)        : 01 10 01 rrrr aaaa aaaa : 0x19raa
--  imem(a)=is        : 01 10 10 aaaa aaaa ssss : 0x1Aaas
--  dmem(a)=ds        : 01 10 11 aaaa aaaa ssss : 0x1Baas

--  ir=imem(is+o)     : 01 11 00 rrrr oooo ssss : 0x1Cros
--  dr=dmem(is+o)     : 01 11 01 rrrr oooo ssss : 0x1Dros
--  imem(is+o)=it     : 01 11 10 oooo tttt ssss : 0x1Eots
--  dmem(is+o)=dt     : 01 11 11 oooo tttt ssss : 0x1Fots

--  alu_op            : 1* ** ** rrrr tttt ssss : -------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


library work;
use work.defines.ALL;

entity CTRL is
    generic(
        D_MEM_ADDR_WIDTH : integer := 10;
        I_MEM_ADDR_WIDTH : integer := 10;
        I_REG_ADDR_WIDTH : integer := 4
    );  
    port ( clk              : in  std_logic;
           res              : in  std_logic;
           nmi              : in  std_logic;
           --I-RAM-Port
           imem_addr_out    : out std_logic_vector(I_MEM_ADDR_WIDTH-1 downto 0);
           imem_data_in     : in  std_logic_vector(I_DATA_WIDTH-1 downto 0);
           imem_data_out    : out std_logic_vector(I_DATA_WIDTH-1 downto 0);
           imem_we_out      : out std_logic;
           imem_en_out      : out std_logic;
           -- D Mem Port
           dmem_we_out      : out std_logic;
           dmem_addr_out    : out std_logic_vector(D_MEM_ADDR_WIDTH-1 downto 0);
           -- IO-Selektion
           io_ctrl_we       : out std_logic;
           io_ctrl_re       : out std_logic;
           io_alu_we        : out std_logic;
           io_alu_re        : out std_logic;
           io_addr          : out std_logic_vector(3 downto 0);
           io_out           : out std_logic_vector(I_DATA_WIDTH-1 downto 0);
           io_in            : in  std_logic_vector(I_DATA_WIDTH-1 downto 0); -- imout auch io-Schreibdaten
           io_ctrl_full     : in  std_logic;
           io_ctrl_empty    : in  std_logic;
           io_alu_full      : in  std_logic;
           io_alu_empty     : in  std_logic;
           -- ALU
           f_in             : in  std_logic;
           --dreg_wr_ctrl_out : out std_logic;
           IR               : out std_logic_vector(I_DATA_WIDTH-1 downto 0);
           PC_out           : out std_logic );
end CTRL;

architecture behavior of CTRL is

    constant I_DATA_WIDTH_2    : integer := I_DATA_WIDTH/2;

    signal ctrlop      : std_logic_vector(I_DATA_WIDTH-1 downto 0); -- verzögertes data_mem_in
    
    --------- signals for the I-Regbank --------
    signal ireg_di     : std_logic_vector(I_DATA_WIDTH-1 downto 0);  
    signal ireg_do1    : std_logic_vector(I_DATA_WIDTH-1 downto 0);  
    signal ireg_do2    : std_logic_vector(I_DATA_WIDTH-1 downto 0);  
    signal ireg_sub    : std_logic_vector(I_DATA_WIDTH-1 downto 0);
    signal ireg_mult   : std_logic_vector(I_DATA_WIDTH*2-1 downto 0);    
    signal ireg_waddr  : std_logic_vector(I_REG_ADDR_WIDTH-1 downto 0);   
    signal ireg_raddr1 : std_logic_vector(I_REG_ADDR_WIDTH-1 downto 0);   
    signal ireg_raddr2 : std_logic_vector(I_REG_ADDR_WIDTH-1 downto 0);   
    signal ireg_we     : std_logic;

    signal instr_addr2   : std_logic_vector(3 downto 0); -- instr_addr2 <= ctrlop(11 downto 8);
    signal instr_addr1   : std_logic_vector(3 downto 0); -- instr_addr1 <= ctrlop(7  downto 4);
    signal instr_addr0   : std_logic_vector(3 downto 0); -- instr_addr0 <= ctrlop(3  downto 0);

    signal ireg_r  : std_logic_vector(I_REG_ADDR_WIDTH-1 downto 0); -- ireg_r <= ctrlop(11 downto 8); -- rrrr
    signal ireg_t  : std_logic_vector(I_REG_ADDR_WIDTH-1 downto 0); -- ireg_t <= ctrlop(7  downto 4); -- tttt
    signal ireg_s  : std_logic_vector(I_REG_ADDR_WIDTH-1 downto 0); -- ireg_s <= ctrlop(3  downto 0); -- ssss
    
    signal mem_addr      : std_logic_vector(I_DATA_WIDTH-1 downto 0) := (others => '0');
    signal const_n       : std_logic_vector(I_DATA_WIDTH-1 downto 0) := (others => '0');
    signal prevPC        : std_logic_vector(I_MEM_ADDR_WIDTH-1 downto 0);
    signal PC            : std_logic_vector(I_MEM_ADDR_WIDTH-1 downto 0); 
    signal jump_addr     : std_logic_vector(I_MEM_ADDR_WIDTH-1 downto 0); 
    signal prev_ireg_r   : std_logic_vector(I_REG_ADDR_WIDTH-1 downto 0); -- für die multiplikaton
    signal irq           : std_logic;
    signal f             : std_logic:='1';
    signal inst          : instruction;
    signal prev_inst     : instruction;
    signal stall         : std_logic;
    signal prev_stall    : std_logic;
    signal imem_en       : std_logic;
    signal n_sign        : std_logic_vector(I_DATA_WIDTH_2 downto 0);
    
    signal io_ctrl_we_sig : std_logic;
    signal io_ctrl_re_sig : std_logic;
    signal io_alu_we_sig  : std_logic;
    signal io_alu_re_sig  : std_logic;
    

    constant pc_zeros   : std_logic_vector(I_DATA_WIDTH-I_MEM_ADDR_WIDTH-1 downto 0)      := (others => '0');
    constant mem_zeros  : std_logic_vector(I_DATA_WIDTH_2-1 downto 0)                     := (others => '0'); -- for const_n
    constant mem_ones   : std_logic_vector(I_DATA_WIDTH_2 downto 0)                       := (others => '1');


begin

    ireg_r <= ctrlop(11 downto 8); -- rrrr
    ireg_t <= ctrlop(7  downto 4); -- tttt
    ireg_s <= ctrlop(3  downto 0); -- ssss
    
    instr_addr2 <= ctrlop(11 downto 8);
    instr_addr1 <= ctrlop(7  downto 4);
    instr_addr0 <= ctrlop(3  downto 0);

    PC_out <= prevPC(I_MEM_ADDR_WIDTH-1);
    
    I_REGBANK: entity work.REGBANK
    generic map (I_DATA_WIDTH, I_REG_ADDR_WIDTH) 
    port map ( wclk => clk,
               we => ireg_we, 
               di => ireg_di, 
               do1 => ireg_do1,  --t
               do2 => ireg_do2,  --s
               wad => ireg_waddr,
               rad1 => ireg_raddr1, --t
               rad2 => ireg_raddr2); -- s

-------------------------------------------------------------------------------------------------------------------------------------------

    IR  <= ctrlop;

-- müsste bei vliw in die alu ausgelagert werden.

--  dreg_wr_ctrl_out <= '1' when prev_inst = dt_eq_dmem_is_add_o or prev_inst = dt_eq_dmem_a or prev_inst = dt_eq_io else
--                      '0';

----- DMEM & IMEM
-------------------------------------------------------------------------------------------------------------------------------------------
 
    mem_addr <= mem_ones & instr_addr1 & instr_addr0                            when inst = ir_eq_imem_a        or inst = dr_eq_dmem_a        else 
                mem_ones & instr_addr2 & instr_addr0                            when inst = imem_a_eq_is        or inst = dmem_a_eq_ds        else              
                std_logic_vector(unsigned(ireg_do2) + unsigned(instr_addr1))    when inst = ir_eq_imem_is_add_o or inst = dr_eq_dmem_is_add_o else
                std_logic_vector(unsigned(ireg_do2) + unsigned(instr_addr2));

 -- store or load index regs into or from memory, transfer ireg from or to memory
 -- TODO: weswegen das prev_stall = 0
    stall <= '1' when prev_stall = '0' and ( inst = imem_a_eq_is or  inst = imem_is_add_o_eq_it or inst = ir_eq_imem_a or inst = ir_eq_imem_is_add_o) else
             '0';


    imem_addr_out <= mem_addr(I_MEM_ADDR_WIDTH-1 downto 0) when stall = '1' else
    --imem_addr_out <= mem_addr(I_MEM_ADDR_WIDTH-1 downto 0) when inst = imem_a_eq_is or  inst = imem_is_add_o_eq_it else
                     PC;

    dmem_addr_out <= mem_addr(D_MEM_ADDR_WIDTH-1 downto 0);


    imem_we_out  <= '1' when (inst = imem_a_eq_is or inst = imem_is_add_o_eq_it) and stall = '1' else
                    '0';
                  
    dmem_we_out  <=  '1' when inst = dmem_a_eq_ds or inst = dmem_is_add_o_eq_dt else
                     '0';                   
 
    imem_data_out <= ireg_do1;
    


    imem_en <= '0' when (io_ctrl_we_sig = '1' and io_ctrl_full  = '1') or
                        (io_alu_we_sig  = '1' and io_alu_full   = '1') or  
                        (io_ctrl_re_sig = '1' and io_ctrl_empty = '1') or
                        (io_alu_re_sig  = '1' and io_alu_empty  = '1') else
               '1';
            
    imem_en_out <= imem_en;
--- IO 
-------------------------------------------------------------------------------------------------------------------------------------------

    io_addr <= instr_addr2;

    io_out  <= ireg_do1;

    io_ctrl_we_sig  <= '1' when inst = io_ops and ctrlop(6) = '1' else -- inst = io_a_eq_is and iordy = '1'
                       '0';
           
    io_alu_we_sig   <= '1' when inst = io_ops and ctrlop(7) = '1' else -- inst = io_a_eq_ds and iordy = '1'
                       '0';
         
 
    io_ctrl_re_sig  <= '1' when inst = io_ops and ctrlop(4) = '1' else -- inst = ir_eq_io
                       '0';


    io_alu_re_sig   <= '1' when inst = io_ops and ctrlop(5) = '1' else -- inst = dr_eq_io
                       '0';


    io_ctrl_we <= io_ctrl_we_sig;
           
    io_alu_we <= io_alu_we_sig;
 
    io_ctrl_re <= io_ctrl_re_sig;

    io_alu_re <= io_alu_re_sig;


--- IREG 
-------------------------------------------------------------------------------------------------------------------------------------------

    n_sign  <= (others=>ctrlop(12));
    const_n <= n_sign & ctrlop(7 downto 0);
  

    ireg_di <=  ireg_mult(I_DATA_WIDTH-1 downto 0)                          when prev_inst = ir_eq_it_mul_is             else
                imem_data_in                                                when prev_inst = ir_eq_imem_a or prev_inst = ir_eq_imem_is_add_o else -- prev_stall = '1'
                std_logic_vector(unsigned(pc_zeros & prevPC)+ 1)            when inst = call_a                           else
                ireg_do2(I_DATA_WIDTH-1 downto I_DATA_WIDTH_2) & ctrlop(12) & ctrlop(7 downto 0)  when inst = ir_l_eq_n  else
                            ctrlop(12) & ctrlop(7 downto 0) & ireg_do2(I_DATA_WIDTH_2-1 downto 0) when inst = ir_h_eq_n  else
                            mem_zeros & ctrlop(12) & ctrlop(7 downto 0)     when inst = ir_eq_n                          else
                io_in                                                       when inst = io_ops and ctrlop(4) = '1'       else --ir_eq_io
                std_logic_vector(unsigned(ireg_do1) + 1)                    when inst = ijnz_a                           else
                std_logic_vector(signed(ireg_do2) + signed(const_n))        when inst = ir_add_eq_n                      else
                ireg_sub                                                    when inst = ir_eq_it_sub_is                  else            
                std_logic_vector(unsigned(ireg_do1) + unsigned(ireg_do2))   when inst = ir_eq_it_add_is                  else
                ireg_do1 and ireg_do2                                       when inst = ir_eq_it_and_is                  else
                ireg_do1 or ireg_do2                                        when inst = ir_eq_it_or_is                   else
                ireg_do2(I_DATA_WIDTH-1) & ireg_do2(I_DATA_WIDTH-1 downto 1)when inst = ir_eq_is_bit and ctrlop(4) = '0' else            
                not ireg_do2                                                when inst = ir_eq_is_bit and ctrlop(4) = '1' else
                ireg_do2;
 
 
    mult: process(clk)
    begin
        if( rising_edge(clk) ) then 
            ireg_mult <= std_logic_vector(unsigned(ireg_do1) * unsigned(ireg_do2));
        end if;
    end process;
        
 
    ireg_sub <= std_logic_vector(unsigned(ireg_do1) - unsigned(ireg_do2));

    ireg_we <= '1'  when  prev_inst = ir_eq_imem_a or prev_inst = ir_eq_imem_is_add_o or -- prev_stall = '1'
                                                prev_inst = ir_eq_it_mul_is or -- neu                        
                          (((inst = io_ops and ctrlop(4) = '1') or  --ir_eq_io
                          inst = ir_eq_n              or
                          inst = ir_eq_it_and_is      or
                          inst = ir_eq_it_or_is       or 
                          inst = ir_eq_is_bit         or
                          inst = ir_eq_it_mul_is      or
                          inst = ir_eq_it_add_is      or
                          inst = ir_eq_it_sub_is      or
                          inst = ir_eq_is             or
                          inst = ir_l_eq_n            or
                          inst = ir_h_eq_n            or
                          inst = ir_add_eq_n          or
                          inst = call_a               or
                          inst = ijnz_a               or 
                          (inst = jmp_rel_ops and ctrlop(6) = '1') or
                          (inst = jmp_rel_ops and ctrlop(7) = '1') ) and prev_stall = '0') else -- warum prev_stall
               '0'; 

    ireg_waddr <= prev_ireg_r         when prev_inst = ir_eq_it_mul_is or prev_inst = ir_eq_imem_a or prev_inst = ir_eq_imem_is_add_o else --prev_stall = '1' else
                  (others => '0')     when inst = ijnz_a or (inst = jmp_rel_ops and ctrlop(7) = '1') else 
                  (others => '1')     when inst = call_a or (inst = jmp_rel_ops and ctrlop(6) = '1') else
                  ireg_r;

    ireg_raddr1 <= (others=>'0')     when inst = ijnz_a or (inst = jmp_rel_ops and ctrlop(7) = '1') else
                   ireg_t;

    ireg_raddr2 <= ireg_r            when inst = ir_add_eq_n or inst = ir_l_eq_n or inst = ir_h_eq_n else
                   ireg_s;

--- CTRL
-------------------------------------------------------------------------------------------------------------------------------------------
                  -- ctrlop(13 downto 0)
    jump_addr  <= ctrlop(I_MEM_ADDR_WIDTH -1 downto 0);
    
    PC <= prevPC                                when  prev_stall = '1' else -- inst = imem_a_eq_is or  inst = imem_is_add_o_eq_it else
          jump_addr                             when  inst = jmp_a  or inst = call_a or (inst = cjf_a and f = '1') or (inst = ijnz_a and ireg_do1(17) = '1')  else
          ireg_do2(I_MEM_ADDR_WIDTH-1 downto 0) when  inst = jmp_rel_ops and ( ctrlop(4) = '1' or                -- rts oder jmp_is;
                                                                              (ctrlop(5) = '1' and f = '1') or   -- cjf_is
                                                                               ctrlop(6) = '1' or                -- call_is
                                                                              (ctrlop(7) = '1' and ireg_do1(17) = '1')) else   -- ijnz_is
          std_logic_vector(unsigned(prevPC) + 1);

 
    PC_INC:
    process(clk)
    begin
        if rising_edge(clk) then
            if res = '1' then
                prevPC <= (others => '0');
            elsif imem_en = '1' then
                prevPC <= PC;
            end if;
        end if;
    end process;
 
 
 F_BIT:
 process(clk)
 begin
    if rising_edge(clk) then
        if res = '1' then
            f <= '0';
        elsif imem_en = '1' then
            if ctrlop(17)='1'                            then f <= f_in;
            elsif inst = f_it_op_is and  ctrlop(8) = '1' then f <= ireg_sub(I_DATA_WIDTH-1);     -- >
            elsif inst = f_it_op_is and  ctrlop(9) = '1' then f <= not ireg_sub(I_DATA_WIDTH-1); -- <=
            else f <= '0';
            end if; 
        end if; -- rising 
    end if;-- res
 end process;
 
-------------------------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------------------------

    REGISTERs: 
    process(clk)
    begin
        if rising_edge(clk) then
            if res = '1' then
                --prev_ireg_t <= (others => '0');
                prev_ireg_r <= (others => '0');  
                --ctrlop    <= "00"&x"0022"; --jmp 0x22
                ctrlop    <= (others => '0');
                --irq       <= '0';
                prev_stall     <= '0';
            elsif imem_en = '1' then
                --prev_ireg_t <= ireg_t;
                prev_ireg_r <= ireg_r; -- für die multiplikaton
                prev_inst <= inst;
                prev_stall <= stall;
                --irq <= nmi;
                -- fehler, wenn stall und nmi gleichzeitig 
                -- oder stall 1 takt hinter nmi -> nmi filtern
                -- fehler, wenn irq direkt nach call kommt und i7 nicht gesichert ist
                if prev_stall = '0' then --if stall = '0' then --if prev_stall = '0' then
                    ctrlop <= imem_data_in;
                    --if nmi='1'    then      ctrlop <= "010000000000000000";-- call & irq_zeros & isr_addr;
                    --elsif irq='1' then      ctrlop <= "110100100000001000";
                    --else                    ctrlop <= imem_data_in;
                    --end if; -- nmi
                end if; -- prev_stall
            end if; -- imem_en
        end if; -- rise_clk
    end process;






    Decode : process (ctrlop)
    begin
        --CTRLOPS   
        case ctrlop(17 downto 14) is
            --JMPS--------------------------------------------------------------------------------
            when X"0" => 
                case ctrlop(13 downto 12) is
                    when "00"    => inst <= jmp_a;                     --  jmp a             : 00 00 00 aaaa aaaa aaaa : 0x00aaa
                    when "01"    => inst <= cjf_a;                     --  cjf a             : 00 00 01 aaaa aaaa aaaa : 0x01aaa
                    when "10"    => inst <= call_a;                    --  call a            : 00 00 10 aaaa aaaa aaaa : 0x02aaa
                    when others  => inst <= ijnz_a;                    --  ijnz a            : 00 00 11 aaaa aaaa aaaa : 0x03aaa
                end case;
            when X"1" => 
                case ctrlop(13) is
                    when '0'     => inst <= jmp_rel_ops;               --  jmp is            : 00 01 0* **** 0001 ssss : 0x04*1s // wenn tttt=0xF -> rts
                                                                       --  cjf is            : 00 01 0* **** 0010 ssss : 0x04*2s
                                                                       --  call is           : 00 01 0* **** 0100 ssss : 0x04*4s
                                                                       --  ijnz is           : 00 01 0* **** 1000 ssss : 0x04*8s
                    when others  => inst <= io_ops;                    --  ir = io(a)        : 00 01 1* rrrr 0001 aaaa : 0x06r1a
                                                                       --  dr = io(a)        : 00 01 1* rrrr 0010 aaaa : 0x06r2a
                                                                       --  io(a) = is        : 00 01 1* aaaa 0100 ssss : 0x06a4s
                                                                       --  io(a) = ds        : 00 01 1* aaaa 1000 ssss : 0x06a8s
                end case;               
            when X"2" => 
                case ctrlop(13 downto 12) is
                    when "00"   => inst <= f_it_op_is;                --  f  = it > is      : 00 10 00 0001 tttt ssss : 0x081ts
                                                                      --  f  = it <= is     : 00 10 00 0010 tttt ssss : 0x082ts
                                                                      --  f  = it == is     : 00 10 00 0100 tttt ssss : 0x084ts
                                                                      --  f  = it != is     : 00 10 00 1000 tttt ssss : 0x088ts
                                                                      
                    when "01"   => inst <= ir_eq_it_and_is;           --  ir = it&is        : 00 10 01 rrrr tttt ssss : 0x09rts
                    when "10"   => inst <= ir_eq_it_or_is;            --  ir = it|is        : 00 10 10 rrrr tttt ssss : 0x0Arts
                    when others => inst <= ir_eq_is_bit;              --  ir = is>>1        : 00 10 11 rrrr ***0 ssss : 0x0Br0s
                                                                      --  ir = ~is          : 00 10 11 rrrr ***1 ssss : 0x0Br1s 
                end case;
            --ARITH--------------------------------------------------------------------------------
            when X"3" =>
                case ctrlop(13 downto 12) is
                    when "00"   => inst <= ir_eq_it_mul_is;           --  ir = it*is        : 00 11 00 rrrr tttt ssss : 0x0Crts
                    when "01"   => inst <= ir_eq_it_add_is;           --  ir = it+is        : 00 11 01 rrrr tttt ssss : 0x0Drts
                    when "10"   => inst <= ir_eq_it_sub_is;           --  ir = it-is        : 00 11 10 rrrr tttt ssss : 0x0Erts
                    when others => inst <= ir_eq_is;                  --  ir = is           : 00 11 11 rrrr **** ssss : 0x0Fr*s
                end case;
                --SET_REG--------------------------------------------------------------------------------
            when X"4" =>
                case ctrlop(13) is
                    when '0'    => inst <= ir_l_eq_n;                 --  ir.l = n          : 01 00 0n rrrr nnnn nnnn : 0x10rnn
                    when others => inst <= ir_h_eq_n;                 --  ir.h = n          : 01 00 1n rrrr nnnn nnnn : 0x12rnn
                end case;
            when X"5" =>
                case ctrlop(13) is
                    when '0'    => inst <= ir_add_eq_n;               --  ir += n           : 01 01 0n rrrr nnnn nnnn : 0x14rnn 
                    when others => inst <= ir_eq_n;                   --  ir  = n           : 01 01 1n rrrr nnnn nnnn : 0x16rnn
                end case;
            --ST&LD_ABS--------------------------------------------------------------------------------
            when X"6" =>
                case ctrlop(13 downto 12) is
                    when "00" => inst <= ir_eq_imem_a;              --  ir=imem(a)        : 01 10 00 rrrr aaaa aaaa : 0x18raa
                    when "01" => inst <= dr_eq_dmem_a;              --  dr=dmem(a)        : 01 10 01 rrrr aaaa aaaa : 0x19raa
                    when "10" => inst <= imem_a_eq_is;              --  imem(a)=is        : 01 10 10 aaaa aaaa ssss : 0x1Aaas
                    when others => inst <= dmem_a_eq_ds;            --  dmem(a)=ds        : 01 10 11 aaaa aaaa ssss : 0x1Baas
                end case;
            --LD_REL_IMEM-------------------------------------------------------------------------------- 
            when X"7" =>
                case ctrlop(13 downto 12) is
                    when "00" => inst <= ir_eq_imem_is_add_o;        --  ir=imem(is+o)     : 01 11 00 rrrr oooo ssss : 0x1Cros
                    when "01" => inst <= dr_eq_dmem_is_add_o;        --  dr=dmem(is+o)     : 01 11 01 rrrr oooo ssss : 0x1Dros
                    when "10" => inst <= imem_is_add_o_eq_it;        --  imem(is+o)=it     : 01 11 10 oooo tttt ssss : 0x1Eots
                    when others => inst <= dmem_is_add_o_eq_dt;      --  dmem(is+o)=dt     : 01 11 11 oooo tttt ssss : 0x1Fots
                end case;
            when others => 
                inst <= no_op;
        end case;
    end process;

end behavior;
