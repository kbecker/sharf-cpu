--Copyright (C) 2013 Cem Bassoy.
--
--This file is part of the Sharf CPU.
--
--Sharf CPU is free software: you can redistribute it and/or modify
--it under the terms of the GNU General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.
--
--Sharf CPU is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU General Public License for more details.
--
--You should have received a copy of the GNU General Public License
--along with Sharf CPU.  If not, see <http:--www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity REGBANK is
	generic(
		REG_DATA_WIDTH : integer :=18;
		REG_ADDR_WIDTH : integer := 4
	);
    Port ( wclk : in  std_logic; -- write clock
           we   : in  std_logic; -- write enable
           di   : in  std_logic_vector (REG_DATA_WIDTH-1 downto 0);
           do1  : out std_logic_vector (REG_DATA_WIDTH-1 downto 0);
           do2  : out std_logic_vector (REG_DATA_WIDTH-1 downto 0);
           wad  : in  std_logic_vector (REG_ADDR_WIDTH-1 downto 0);  -- write address
           rad1 : in  std_logic_vector (REG_ADDR_WIDTH-1 downto 0);  -- read address 1
           rad2 : in  std_logic_vector (REG_ADDR_WIDTH-1 downto 0)); -- read address 2
end REGBANK;

architecture behavior of REGBANK is

	type mem_type is array (2**REG_ADDR_WIDTH-1 downto 0) of std_logic_vector (REG_DATA_WIDTH-1 downto 0); 
	signal  reg: mem_type;

	begin
		process (wclk, reg)
		begin
			if (wclk'event and wclk = '1') then
				if (we = '1') then
					reg(to_integer(unsigned(wad))) <= di;
				end if;
			end if;
		end process;

		do1 <= reg(to_integer(unsigned(rad1)));
		do2 <= reg(to_integer(unsigned(rad2)));
	
end behavior;

