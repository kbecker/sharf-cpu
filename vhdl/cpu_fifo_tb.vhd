--Copyright (C) 2013 Cem Bassoy.
--
--This file is part of the Sharf CPU.
--
--Sharf CPU is free software: you can redistribute it and/or modify
--it under the terms of the GNU General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.
--
--Sharf CPU is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU General Public License for more details.
--
--You should have received a copy of the GNU General Public License
--along with Sharf CPU.  If not, see <http:--www.gnu.org/licenses/>.


library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

library work;
use work.defines.ALL;

entity CPU_FIFO_TB IS
end CPU_FIFO_TB;

architecture behavior OF CPU_FIFO_TB IS 

	--Inputs
   signal clk :	std_logic := '0';
   signal res :	std_logic := '0';
   signal nmi :	std_logic := '0';   
   signal io_ctrl_we1,    io_ctrl_we2    : std_logic;
   signal io_ctrl_re1,    io_ctrl_re2    : std_logic;
   signal io_ctrl_full1,  io_ctrl_full2  : std_logic;
   signal io_ctrl_empty1, io_ctrl_empty2 : std_logic;
   signal io_alu_full1,   io_alu_full2   : std_logic;
   signal io_alu_empty1,  io_alu_empty2  : std_logic;   
   signal io_alu_we1,     io_alu_we2     : std_logic;
   signal io_alu_re1,     io_alu_re2     : std_logic;
   signal io_ctrl_out1,   io_ctrl_out2   : std_logic_vector(I_DATA_WIDTH-1 downto 0);
   signal io_ctrl_in1,    io_ctrl_in2    : std_logic_vector(I_DATA_WIDTH-1 downto 0);
   signal io_alu_out1,    io_alu_out2    : std_logic_vector(D_DATA_WIDTH-1 downto 0);
   signal io_alu_in1,     io_alu_in2     : std_logic_vector(D_DATA_WIDTH-1 downto 0); 


begin

	CPU1: entity work.CPU
	generic map( "iram1.dat", "dram1.dat")	
	port map( clk => clk,
	          res => res,
	          nmi => nmi,
	          PC_out => open,
	          io_ctrl_we => io_ctrl_we1,
	          io_ctrl_re => io_ctrl_re1,
	          io_alu_we => io_alu_we1,
	          io_alu_re => io_alu_re1,
	          io_addr => open,
	          io_ctrl_out => io_ctrl_out1,
	          io_ctrl_in => io_ctrl_in1,
	          io_ctrl_full => io_ctrl_full1,
	          io_ctrl_empty => io_ctrl_empty1,
	          io_alu_out => io_alu_out1,
	          io_alu_in => io_alu_in1,
	          io_alu_full => io_alu_full1,
	          io_alu_empty => io_alu_empty1);
	
	CPU2: entity work.CPU
	generic map( "iram2.dat", "dram2.dat")	
	port map( clk => clk,
	          res => res,
	          nmi => nmi,
	          PC_out => open,
	          io_ctrl_we => io_ctrl_we2,
	          io_ctrl_re => io_ctrl_re2,
	          io_alu_we => io_alu_we2,
	          io_alu_re => io_alu_re2,
	          io_addr => open,
	          io_ctrl_out => io_ctrl_out2,
	          io_ctrl_in => io_ctrl_in2,
	          io_ctrl_full => io_ctrl_full2,
	          io_ctrl_empty => io_ctrl_empty2,
	          io_alu_out => io_alu_out2,
	          io_alu_in => io_alu_in2,
	          io_alu_full => io_alu_full2,
	          io_alu_empty => io_alu_empty2);
	
	FIFO_CTRL_12: entity work.FIFO
	generic map( FIFO_ADDR_WIDTH, I_DATA_WIDTH)
  port map(
		clk => clk,
		res => res,
  data_in => io_ctrl_out1,
 data_out => io_ctrl_in2,
   isFull => io_ctrl_full1,
  isEmpty => io_ctrl_empty2,
 write_en => io_ctrl_we1,
  read_en => io_ctrl_re2);

	FIFO_CTRL_21: entity work.FIFO
	generic map( FIFO_ADDR_WIDTH, I_DATA_WIDTH)
  port map(
		clk => clk,
		res => res,
  data_in => io_ctrl_out2,
 data_out => io_ctrl_in1,
   isFull => io_ctrl_full2,
  isEmpty => io_ctrl_empty1,
 write_en => io_ctrl_we2,
  read_en => io_ctrl_re1);

	FIFO_ALU_12: entity work.FIFO
	generic map( FIFO_ADDR_WIDTH, I_DATA_WIDTH)
  port map(
		clk => clk,
		res => res,
  data_in => io_alu_out1,
 data_out => io_alu_in2,
   isFull => io_alu_full1,
  isEmpty => io_alu_empty2,
 write_en => io_alu_we1,
  read_en => io_alu_re2);

	FIFO_ALU_21: entity work.FIFO
	generic map( FIFO_ADDR_WIDTH, I_DATA_WIDTH)
  port map(
		clk => clk,
		res => res,
  data_in => io_alu_out2,
 data_out => io_alu_in1,
   isFull => io_alu_full2,
  isEmpty => io_alu_empty1,
 write_en => io_alu_we2,
  read_en => io_alu_re1);

	clock : process
	begin
		clk <= not clk;
		wait for 10 ns;
	end process;

	tb : process
	begin
		res <= '1';
		nmi <= '0';
		wait for 100 ns;
		res <= '0';
		wait; -- will wait forever
	end process;
		
	
end;
