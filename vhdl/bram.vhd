--Copyright (C) 2013 Cem Bassoy.
--
--This file is part of the Sharf CPU.
--
--Sharf CPU is free software: you can redistribute it and/or modify
--it under the terms of the GNU General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.
--
--Sharf CPU is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU General Public License for more details.
--
--You should have received a copy of the GNU General Public License
--along with Sharf CPU.  If not, see <http:--www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;
use std.textio.all;

entity BRAM is
	generic(
		MEM_ADDR_WIDTH : integer;
		MEM_DATA_WIDTH : integer;
		MEM_NAME : string := "iram.dat"
	);
    Port ( clk   : in  std_logic;
           en    : in  std_logic;
           we    : in  std_logic;
           addr  : in  std_logic_vector (MEM_ADDR_WIDTH-1 downto 0);
           di    : in  std_logic_vector (MEM_DATA_WIDTH-1 downto 0);
           do    : out std_logic_vector (MEM_DATA_WIDTH-1 downto 0)
			  );
end BRAM;

architecture Behavioral of BRAM is

	type mem_type is array (0 to 2**MEM_ADDR_WIDTH-1) of std_logic_vector (MEM_DATA_WIDTH-1 downto 0); 
	impure function InitRamFromFile (RamFileName : in string) return mem_type is
		FILE RamFile : text is in RamFileName;
		variable RamFileLine : line;
		variable RAM : mem_type;
		begin
			for i in mem_type'range loop
				readline (RamFile, RamFileLine);
				read (RamFileLine, RAM(i));
			end loop;
			return RAM;
		end function;
	signal ram : mem_type := InitRamFromFile(MEM_NAME);
	

	begin
		process (clk)
		begin
			if (clk'event and clk = '1') then
				if (en ='1') then
					if (we = '1') then
						ram(to_integer(unsigned(addr))) <= di;	
					else 	
						do <= ram(to_integer(unsigned(addr))); 
					end if;	
				end if;
			end if;
		end process;
				

end Behavioral;



