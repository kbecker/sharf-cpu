--Copyright (C) 2013 Cem Bassoy.
--
--This file is part of the Sharf CPU.
--
--Sharf CPU is free software: you can redistribute it and/or modify
--it under the terms of the GNU General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.
--
--Sharf CPU is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU General Public License for more details.
--
--You should have received a copy of the GNU General Public License
--along with Sharf CPU.  If not, see <http:--www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.defines.ALL;

entity CPU is
	generic( IMEM_NAME  : string := "../vhdl/iram.dat";
	         DMEM_NAME : string := "../vhdl/dram.dat";
	         D_DATA_WIDTH     : integer := 32;
	         I_MEM_ADDR_WIDTH : integer := 10;
	         D_MEM_ADDR_WIDTH : integer := 10;
	         I_REG_ADDR_WIDTH : integer := 4;
	         D_REG_ADDR_WIDTH : integer := 4
	       );
	port( clk           : in  std_logic;  -- 25 MHz .. 100 MHz
	      res           : in  std_logic;  -- als cpu-reset(H) ben.
	      nmi           : in  std_logic;
	      PC_out        : out std_logic;
	      io_ctrl_we    : out std_logic;
	      io_ctrl_re    : out std_logic;
	      io_alu_we     : out std_logic;
	      io_alu_re     : out std_logic;
	      io_addr       : out std_logic_vector(3 downto 0);		
	      io_ctrl_out   : out std_logic_vector(I_DATA_WIDTH-1 downto 0);
	      io_ctrl_in    : in  std_logic_vector(I_DATA_WIDTH-1 downto 0);
	      io_ctrl_full  : in  std_logic;
	      io_ctrl_empty : in  std_logic;
	      io_alu_out    : out std_logic_vector(D_DATA_WIDTH-1 downto 0);
	      io_alu_in     : in  std_logic_vector(D_DATA_WIDTH-1 downto 0);
	      io_alu_full   : in  std_logic;
	      io_alu_empty  : in  std_logic);
end CPU;

architecture behavior of CPU is

	signal iadr       : std_logic_vector(I_MEM_ADDR_WIDTH-1 downto 0); 			
	signal imem_out   : std_logic_vector(I_DATA_WIDTH-1 downto 0);
	signal imem_in    : std_logic_vector(I_DATA_WIDTH-1 downto 0);
	signal dadr       : std_logic_vector(D_MEM_ADDR_WIDTH-1 downto 0); 			
	signal dmem_out   : std_logic_vector(D_DATA_WIDTH-1 downto 0);
	signal dmem_in 	  : std_logic_vector(D_DATA_WIDTH-1 downto 0);
	signal dmem_we    : std_logic;
	signal imem_we    : std_logic;
	signal imem_en    : std_logic;	
	signal f          : std_logic;
	signal ir         : std_logic_vector(I_DATA_WIDTH-1 downto 0);
 
---------------------

	constant imem_di : std_logic_vector(I_DATA_WIDTH-1 downto 0) := (others => '0');
  

---------------------

begin

	i_CTRL: entity work.CTRL 
	generic map(D_MEM_ADDR_WIDTH, I_MEM_ADDR_WIDTH, I_REG_ADDR_WIDTH)
	port map( clk => clk, 
	          res => res, 
	          nmi => nmi,
	          imem_addr_out	=> iadr, 
	          imem_data_in => imem_out, 
	          imem_data_out	=> imem_in, 
	          imem_we_out => imem_we,
	          imem_en_out => imem_en,
	          dmem_we_out => dmem_we,
	          dmem_addr_out => dadr,
	          io_ctrl_we => io_ctrl_we,
	          io_ctrl_re => io_ctrl_re,
	          io_alu_we => io_alu_we,
	          io_alu_re => io_alu_re,
	          io_addr => io_addr,
	          io_out => io_ctrl_out,
	          io_in => io_ctrl_in,
	          io_ctrl_full => io_ctrl_full,
	          io_ctrl_empty => io_ctrl_empty,
	          io_alu_full => io_alu_full,
	          io_alu_empty => io_alu_empty,
	          f_in => f,
	          IR => ir,
	          PC_out => PC_out);

	I_RAM: entity work.BRAM
	generic map(I_MEM_ADDR_WIDTH, I_DATA_WIDTH, IMEM_NAME)
	port map (clk => clk, 
	          en => imem_en, 
	          we => imem_we, 
	          addr => iadr,
	          di => imem_in,
	          do => imem_out );								
         
	d_ALU:  entity work.ALU
	generic map( D_DATA_WIDTH, D_REG_ADDR_WIDTH)
	port map ( clk => clk, 
	           res => res, 
	           aluop	=> ir, 
	           fout	=> f,
	           io_out => io_alu_out,
	           io_in => io_alu_in,
	           dmem_data_in	=> dmem_out, 
	           dmem_data_out	=> dmem_in );

	
	D_RAM: entity work.BRAM 
	generic map(D_MEM_ADDR_WIDTH, D_DATA_WIDTH, DMEM_NAME)
	port map ( clk  => clk, 
	           en => '1', 
	           we => dmem_we, 
	           addr => dadr,
	           di => dmem_in,
	           do => dmem_out );

end behavior;
