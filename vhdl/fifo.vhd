--Copyright (C) 2013 Cem Bassoy.
--
--This file is part of the Sharf CPU.
--
--Sharf CPU is free software: you can redistribute it and/or modify
--it under the terms of the GNU General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.
--
--Sharf CPU is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU General Public License for more details.
--
--You should have received a copy of the GNU General Public License
--along with Sharf CPU.  If not, see <http:--www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity DPRAM is
	generic( MEM_ADDR_WIDTH : integer := 4; MEM_DATA_WIDTH : integer := 18 );
    Port ( wclk        : in  std_logic;
           we          : in  std_logic;			 
           wr_addr     : in  std_logic_vector (MEM_ADDR_WIDTH-1 downto 0);
           rd_addr     : in  std_logic_vector (MEM_ADDR_WIDTH-1 downto 0);
           data_in     : in  std_logic_vector (MEM_DATA_WIDTH-1 downto 0);
           --data_wr_out : out std_logic_vector (MEM_DATA_WIDTH-1 downto 0);  -- data in the memory addressed by write address (spo)
           data_rd_out : out std_logic_vector (MEM_DATA_WIDTH-1 downto 0)   -- data in the memory addressed by read address (dpo)
			  );
end DPRAM;

architecture Behavioral of DPRAM is

subtype row is std_logic_vector(MEM_DATA_WIDTH-1 downto 0);
type mem_type is array (0 to 2**MEM_ADDR_WIDTH-1) of row;--BIT_VECTOR (MEM_DATA_WIDTH-1 downto 0);
signal ram : mem_type;

begin
	process (wclk)
	begin
			if rising_edge(wclk) then
				if (we = '1') then
					ram(conv_integer(wr_addr)) <= data_in;					
				end if; 	
				--data_wr_out <= ram(conv_integer(wr_addr));				
				
			end if;
			data_rd_out <= ram(conv_integer(rd_addr));
	end process;
end Behavioral;



entity FIFO is
	generic( MEM_ADDR_WIDTH : integer := 4; MEM_DATA_WIDTH : integer := 18 );
    Port ( clk      : in  std_logic;
           res      : in  std_logic;
           data_in  : in  std_logic_vector (MEM_DATA_WIDTH-1 downto 0);
           data_out : out std_logic_vector (MEM_DATA_WIDTH-1 downto 0);
					 isFull   : out std_logic;
					 isEmpty  : out std_logic;
					 write_en : in std_logic;
					 read_en  : in std_logic
			  );
end FIFO;

architecture Behavioral of FIFO is
	
signal wr_pointer  : std_logic_vector( MEM_ADDR_WIDTH-1 downto 0);
signal rd_pointer : std_logic_vector( MEM_ADDR_WIDTH-1 downto 0);
signal wr_addr  : std_logic_vector( MEM_ADDR_WIDTH-1 downto 0);
signal rd_addr  : std_logic_vector( MEM_ADDR_WIDTH-1 downto 0);
signal full,empty    : std_logic;
signal we : std_logic;

begin
	ram: entity work.DPRAM
	generic map( MEM_ADDR_WIDTH, MEM_DATA_WIDTH)
  port map	( wclk => clk,
	              we => we,
           wr_addr => wr_addr,
					 rd_addr => rd_addr, 
					 data_in => data_in,
			 data_rd_out => data_out);


	we <= not full;
				 
	empty <= '1' when wr_pointer = rd_pointer else
	          '0';
	
	full <= '1' when (wr_pointer+1) = rd_pointer else
	         '0';
						 
	wr_addr <= wr_pointer;
	rd_addr <= rd_pointer;
	
	isFull <= full;
	isEmpty <= empty;
	
	

	process (clk,res)
	begin
		if rising_edge(clk) then
			if res = '1' then
				wr_pointer <= (others=>'0');
				rd_pointer <= (others=>'0');
			else			
				if write_en = '1' and full = '0' then					
					wr_pointer <= wr_pointer + 1;
				end if;
				if read_en = '1' and empty = '0' then
					rd_pointer <= rd_pointer + 1;
				end if;
				--elsif write_en = '1' and read_en = '1' and full = '0' and empty = '0'
				--end if;
			end if;
		end if;
	end process;
end Behavioral;




