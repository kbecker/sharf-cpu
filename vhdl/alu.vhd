--Copyright (C) 2013 Cem Bassoy.
--
--This file is part of the Sharf CPU.
--
--Sharf CPU is free software: you can redistribute it and/or modify
--it under the terms of the GNU General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.
--
--Sharf CPU is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU General Public License for more details.
--
--You should have received a copy of the GNU General Public License
--along with Sharf CPU.  If not, see <http:--www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.defines.ALL;

entity ALU is
	generic(
		D_DATA_WIDTH : integer := 32;
		D_REG_ADDR_WIDTH : integer := 4
	);
	port ( clk      : in  std_logic;
	       res      : in  std_logic;
	       aluop    : in  std_logic_vector(I_DATA_WIDTH-1 downto 0);		
	       fout     : out std_logic;
	       io_out   : out std_logic_vector(D_DATA_WIDTH-1 downto 0);
	       io_in    : in  std_logic_vector(D_DATA_WIDTH-1 downto 0);
	      dmem_data_in	: in  std_logic_vector(D_DATA_WIDTH-1 downto 0);		
	      dmem_data_out : out std_logic_vector(D_DATA_WIDTH-1 downto 0));
end ALU;

architecture behavior of ALU is

	signal sum         : std_logic_vector(D_DATA_WIDTH     downto 0);
	signal alu_res     : std_logic_vector(D_DATA_WIDTH-1   downto 0);
	--signal mult        : std_logic_vector(2*D_DATA_WIDTH-1 downto 0);
	signal op2         : std_logic_vector(D_DATA_WIDTH     downto 0);
	signal s_mult      : signed(2*D_DATA_WIDTH-1 downto 0);
	
	signal dreg_di     : std_logic_vector(D_DATA_WIDTH-1 downto 0);
	signal dreg_do1    : std_logic_vector(D_DATA_WIDTH-1 downto 0);
	signal dreg_do2    : std_logic_vector(D_DATA_WIDTH-1 downto 0);	
	signal dreg_waddr  : std_logic_vector(D_REG_ADDR_WIDTH-1 downto 0);
	signal dreg_we     : std_logic;	
	
	signal aluopPrev   : std_logic_vector(I_DATA_WIDTH-1 downto 0)  := (others => '0');
	signal z           : std_logic;
	constant dr_zeros  : std_logic_vector(D_DATA_WIDTH - 9 downto 0)  := (others => '0');
	constant z_zeros   : std_logic_vector(D_DATA_WIDTH     downto 0)  := (others => '0');
	signal dreg_wr_ctrl: std_logic;
	signal dreg_wr_io  : std_logic;
	signal const_n     : std_logic_vector(D_DATA_WIDTH-1 downto 0) := (others => '0');
	signal n_sign      : std_logic_vector(D_DATA_WIDTH-1-9 downto 0);
 
	signal inst             : instruction;
	signal inst_group       : instruction_group;
	
	constant NUM_PIPE_MUL : integer := 2; -- number of pipelinestages for the multiplier = NUM_PIPE_MUL+1
	type reg_type_out is array (NUM_PIPE_MUL-1 downto 0) of std_logic_vector (2*D_DATA_WIDTH-1 downto 0); 
	signal mult: reg_type_out ;

 
begin

	D_REGBANK: entity work.REGBANK 
	generic map(D_DATA_WIDTH, D_REG_ADDR_WIDTH)
	port map (
			wclk => clk, 
			we   => dreg_we, 
			di   => dreg_di, 
			do1  => dreg_do1,
			do2  => dreg_do2, 
			wad  => dreg_waddr,
			rad1 => aluop( 7 downto  4), 
			rad2 => aluop( 3 downto  0)
	);

	-- verzögerung der instruktion für d
	delay_aluop: process (clk,res) 
	begin
		if rising_edge(clk) then
			if res = '1' then
				aluopPrev <= (others => '0');
			else			
				aluopPrev <= aluop;
			end if;
		end if;	
	end process;

--instruct = dr_eq_dmem_a or instruct = dr_eq_dmem_is_add_o	 or instruct dt_eq_io
	
	--  dr=dmem(is+o)     : 01 11 01 rrrr oooo ssss : 0x1Dros
	--  dr=dmem(a)        : 01 10 01 rrrr aaaa aaaa : 0x19raa
	--  dr = io(a)        : 00 01 1* rrrr 0010 aaaa : 0x06r2a
	
	-- hier aupassen dass dt=dmem(..) nicht direkt nach dt = io(..) kommt.
	dreg_wr_ctrl <= '1' when aluopPrev(17 downto 15)="011" and aluopPrev(13 downto 12)="01" else
	                '0';
	                
	dreg_wr_io <= '1' when aluop(17 downto 13)="00011" else 
								'0';
			
	  
-------------------------------------------------------------------------------	
  dmem_data_out <= dreg_do1;
  
	io_out <= dreg_do1;
	

	n_sign  <= (others=>aluop(13));
  const_n <= n_sign & aluop(12) & aluop(7 downto 0);
	
	
	dreg_di <= dmem_data_in  when dreg_wr_ctrl = '1' else	
	           io_in         when dreg_wr_io = '1'   else
	           const_n       when inst = dr_eq_n     else
	           alu_res;

	
	dreg_we <= '1'	when inst = dr_eq_mult_res_lsw   or inst = dr_eq_mult_res_msw or
	                     inst_group = logic_alu_ops  or
	                     inst_group = arith_alu_ops  or 	                     
	                     inst_group = set_dreg_ops   else
	                     (dreg_wr_ctrl or dreg_wr_io);
										
	dreg_waddr  <=  aluopPrev(11 downto 8) when dreg_wr_ctrl='1' else
	                aluop(11 downto 8);

										
	alu_res  <=  mult(NUM_PIPE_MUL-1)(D_DATA_WIDTH-1 downto 0)              when inst = dr_eq_mult_res_lsw  else
	             mult(NUM_PIPE_MUL-1)(2*D_DATA_WIDTH-1 downto D_DATA_WIDTH) when inst = dr_eq_mult_res_msw  else
	             sum(D_DATA_WIDTH-1 downto 0)                when inst_group = arith_alu_ops  else
	             dreg_do1 and dreg_do2                       when inst = dr_eq_dt_and_ds      else
	             dreg_do1 or  dreg_do2                       when inst = dr_eq_dt_or_ds       else
	             not dreg_do2                                when inst = dr_eq_not_ds         else              
	             dreg_do2(D_DATA_WIDTH-1) & dreg_do2(D_DATA_WIDTH-1 downto 1) ;


	op2 <= (dreg_do2(D_DATA_WIDTH-1) & dreg_do2)            when inst = dr_eq_dt_add_ds or inst = dr_eq_ds else -- erweiterung mit dem letzten bit, bei addition
	       std_logic_vector(unsigned(not (dreg_do2(D_DATA_WIDTH-1)& dreg_do2)) + 1); -- invertierung und addition mit 1 für 


	sum <= op2  when inst = dr_eq_ds or  inst = dr_eq_neg_ds  or (aluop(11)='1' and inst_group = cmp_ops) else
	       std_logic_vector(signed(dreg_do1(D_DATA_WIDTH-1) & dreg_do1) + signed(op2));
	
	

	multiplikation: process (clk) is
	begin
		if rising_edge(clk) then
			if res = '1' then
				s_mult <= (others => '0');
				for i in 0 to NUM_PIPE_MUL-1 loop
					mult(i) <= (others => '0');
				end loop;
			else
				s_mult <= signed(dreg_do1) * signed(dreg_do2);
				mult(0) <= std_logic_vector(s_mult);
				for i in 1 to NUM_PIPE_MUL-1 loop
					mult(i) <= mult(i-1);
				end loop;				
				
				--mult1 <= mult2;
			end if;
		end if;
	end process;

	
 z  <=  '1' when sum(D_DATA_WIDTH downto 0) = z_zeros else
				'0';
	
	fout	<=  z                                 when inst = cmp_z   else
						not(z)                            when inst = cmp_nz  else
						sum(D_DATA_WIDTH)     and (not z) when inst = cmp_lz  else --  <
						not sum(D_DATA_WIDTH)             when inst = cmp_gez else --  >=
						sum(D_DATA_WIDTH)                 when inst = dr_eq_dt_add_ds or  inst = dr_eq_dt_sub_ds else
						'0';--dreg_do2(0); wofür war dreg_do2 ?

Decode: process(aluop)
 begin
    case aluop(17 downto 14) is
      -- SIMPLE ALU OPS_1
			when X"8"		=>
				case aluop(13 downto 12) is
					when "00"	  =>	inst <= dr_eq_ds_sr;              -- dr=ds>>1 : 1 000 00 rrrr **** ssss : 0x20r*s
					when "01"	  =>	inst <= dr_eq_not_ds;             -- dr=~ds   : 1 000 01 rrrr **** ssss : 0x21r*s
					when "10"	  =>	inst <= dr_eq_dt_and_ds;			    -- dr=dt&ds : 1 000 10 rrrr tttt ssss : 0x22r*s		
					when others =>	inst <= dr_eq_dt_or_ds;           -- dr=dt|ds : 1 000 11 rrrr tttt ssss : 0x23r*s
				end case;      
        inst_group <= logic_alu_ops;
      -- SIMPLE ALU OPS_2
			when X"9"		=>
				case aluop(13 downto 12) is
					when "00"	  =>	inst <= dr_eq_dt_add_ds;          --	dr=dt+ds : 1 001 00 rrrr tttt ssss : 0x24r*s
					when "01"	  =>	inst <= dr_eq_dt_sub_ds;          --	dr=dt-ds : 1 001 01 rrrr tttt ssss : 0x25r*s
					when "10"	  =>	inst <= dr_eq_ds;				          --	dr=ds    : 1 001 10 rrrr **** ssss : 0x26r*s
					when others	=>	inst <= dr_eq_neg_ds;             --	dr=-ds   : 1 001 11 rrrr **** ssss : 0x27r*s
				end case;      
        inst_group <= arith_alu_ops;
      -- MUL OPS
			when X"A"		=> 
				case aluop(13 downto 12) is         
					when "00"	  =>	inst <= dr_eq_mult_res_lsw;     -- dr=res.l : 1 010 00 rrrr **** **** : 0x28r** 
					when "01"	  =>	inst <= dr_eq_mult_res_msw;     -- dr=res.h : 1 010 01 rrrr **** **** : 0x29r**
					when "10"	  =>	inst <= dt_mul_ds;              -- res = (dt*ds) : 1 010 10 **** tttt ssss : 0x2A*ts
          when others	=> inst <= no_op;
				end case;
        inst_group <= mul_ops;
      -- ACC OPS
--			when X"B"		=>
--				case aluop(13 downto 12) is
--					when "00"	  =>	inst <= acc_eq_n;                -- acc=n      : 1 011 00 nnnn nnnn nnnn : 0x2Cr*s
--					when "01"	  =>	inst <= acc_plus_eq_dt_mul_ds;   -- acc+=dt*ds : 1 011 01 **** tttt ssss : 0x2Dr*s
--					when others	=> inst <= no_op;
--				end case;
--        inst_group <= acc_ops;
      -- SET REG OPS
      when X"C"		=> 
	      inst <= dr_eq_n;                                   -- dr=n     : 1 100 nn rrrr nnnn nnnn : 0x3nrnn						    
        inst_group <= set_dreg_ops;
      -- CMP OPS
      when X"D"		=> 
      	case aluop(13 downto 12) is
					when "00"	  =>	inst <= cmp_z;                   -- f=(dt==ds) : 1 101 00 **** tttt ssss : 0x34r*s
					when "01"	  =>	inst <= cmp_nz;   	             -- f=(dt!=ds) : 1 101 01 **** tttt ssss : 0x35r*s
					when "10"	  =>	inst <= cmp_lz;					         -- f=(dt<ds)  : 1 101 10 **** tttt ssss : 0x36r*s
      		when others	=>	inst <= cmp_gez;				         -- f=(dt>=ds) : 1 101 11 **** tttt ssss : 0x37r*s
				end case;
				inst_group <= cmp_ops;
      when others	=>
        inst_group <= no_group_ops;
        inst       <= no_op;
				end case;
 end process;
 
	
end behavior;	

