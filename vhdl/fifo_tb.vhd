--Copyright (C) 2013 Cem Bassoy.
--
--This file is part of the Sharf CPU.
--
--Sharf CPU is free software: you can redistribute it and/or modify
--it under the terms of the GNU General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.
--
--Sharf CPU is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU General Public License for more details.
--
--You should have received a copy of the GNU General Public License
--along with Sharf CPU.  If not, see <http:--www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.ALL;
library work;
use work.defines.ALL;
--use work.tb_pkg.ALL;

entity FIFO_TB IS
end FIFO_TB;

architecture behavior OF FIFO_TB IS 

--	component FIFO is
--	generic( MEM_ADDR_WIDTH : integer := 4; MEM_DATA_WIDTH : integer := 18 );
--    Port ( clk      : in  std_logic;
--           res      : in  std_logic;
--           data_in  : in  std_logic_vector (MEM_DATA_WIDTH-1 downto 0);
--           data_out : out std_logic_vector (MEM_DATA_WIDTH-1 downto 0);
--					 isFull   : out std_logic;
--					 isEmpty  : out std_logic;
--					 write_en : in std_logic;
--					 read_en  : in std_logic
--			  );
--	end component;
	

	--Inputs
	signal clk :	std_logic := '0';
	signal res :	std_logic := '0';
	signal data_in  : std_logic_vector(I_DATA_WIDTH-1 downto 0);  
	signal data_out : std_logic_vector(I_DATA_WIDTH-1 downto 0);
	signal isFull, isEmpty : std_logic;
	signal write_en, read_en : std_logic;
	signal counter : std_logic_vector(10 downto 0);  


begin

	FIFO1: entity work.FIFO
	generic map( FIFO_ADDR_WIDTH, I_DATA_WIDTH)
  port map(
		clk => clk,
		res => res,
		data_in => data_in,
		data_out => data_out,
		isFull => isFull,
		isEmpty => isEmpty,
		write_en => write_en,
		read_en => read_en);


	clock : process
	begin
		clk <= not clk;
		wait for 10 ns;
	end process;

 -- RESET Process --------------------
	 res_pr : process
	 begin
		res <= '1';
		wait for 60 ns;
		res <= '0';
		wait; -- will wait forever
	 end process;

 -- Generating Inputs for the FIFO---------------
	tb_in : process(clk,res)
	begin
	if (rising_edge(clk)) then
		if(res = '1') then
			data_in <= (others=>'0');
			write_en <= '0';
			read_en <= '0';
		elsif(counter < 16) then
			write_en <= '1';
		  data_in <= data_in +1;
		elsif(counter < 32) then
			write_en <= '0';
			read_en <= '1';
		elsif(counter < 48) then
			write_en <= '1';
			read_en <= '1';
			data_in <= data_in +1;
		end if;
		
	end if;
	end process;
		
		
	count : process(clk,res)
	begin
	if (rising_edge(clk)) then
		if(res = '1') then
			counter <= (others=>'0');
		else
			counter <= counter +1;
		end if;
	end if;
	end process;
	
end;
