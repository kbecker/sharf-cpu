--Copyright (C) 2013 Cem Bassoy.
--
--This file is part of the Sharf CPU.
--
--Sharf CPU is free software: you can redistribute it and/or modify
--it under the terms of the GNU General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.
--
--Sharf CPU is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU General Public License for more details.
--
--You should have received a copy of the GNU General Public License
--along with Sharf CPU.  If not, see <http:--www.gnu.org/licenses/>.

library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package DEFINES is

constant I_DATA_WIDTH  : integer := 18;

type instruction is (
  -- JMPS
  jmp_a,
  call_a,
  cjf_a,
  ijnz_a,
	jmp_rel_ops,
	io_ops,
  -- LD ABS OPS
  ir_eq_imem_a,
  dr_eq_dmem_a,
  -- ST ABS OPS
  imem_a_eq_is,
  dmem_a_eq_ds,
  -- LD MEM REL OPS
  ir_eq_imem_is_add_o,
  dr_eq_dmem_is_add_o,
  -- ST MEM REL OPS
  imem_is_add_o_eq_it,
  dmem_is_add_o_eq_dt,
  -- IREG ARITH OPS
  ir_eq_is,
  ir_eq_it_sub_is,
  ir_eq_it_add_is,
  ir_eq_it_mul_is,
  ir_add_eq_n,
  -- SET IREG.L
  ir_l_eq_n,
	ir_h_eq_n,
	ir_eq_n,
  ir_eq_is_bit,
  -- FBIT
  f_it_op_is,
	
  ir_eq_it_and_is,
  ir_eq_it_or_is,
  -- ALU OP
  -- ARITH ALU OPS
  dr_eq_dt_add_ds,
  dr_eq_dt_sub_ds,
  dr_eq_ds,
  dr_eq_neg_ds,
  -- LOGIC ALU OPS
  dr_eq_ds_sr,
  dr_eq_not_ds,
  dr_eq_dt_and_ds,
  dr_eq_dt_or_ds,
  -- MUL OPS
  --dr_eq_dt_mul_ds_lsw,
  --dr_eq_dt_mul_ds_msw,
  
	dr_eq_mult_res_lsw,
	dr_eq_mult_res_msw,
	dt_mul_ds,
	
	dr_eq_fl_add_sub,
  dr_eq_fl_mul,
  -- ACC OPS
  acc_eq_n,
  acc_plus_eq_dt_mul_ds,
  -- SET REG OPS
  dr_eq_acc_lsw,
  dr_eq_acc_msw,
  dr_eq_n,
  -- CMP OPS
  cmp_z,
  cmp_nz,
  cmp_gez,
  cmp_lz,
  --
  dr_eq_is,
  ir_eq_ds,
  --
  no_op
);

type instruction_group is (
    jmp_ops,
	io_ops, -- for IO operations
    arith_ctrl_ops,
    set_ireg_ops,
    abs_mem_ops,
    logic_alu_ops,
    arith_alu_ops,
    mul_ops,
    acc_ops,
    cmp_ops,
    set_dreg_ops,
    no_group_ops,
    rel_mem
);	

end DEFINES;

