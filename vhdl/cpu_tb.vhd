--Copyright (C) 2013 Cem Bassoy.
--
--This file is part of the Sharf CPU.
--
--Sharf CPU is free software: you can redistribute it and/or modify
--it under the terms of the GNU General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.
--
--Sharf CPU is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU General Public License for more details.
--
--You should have received a copy of the GNU General Public License
--along with Sharf CPU.  If not, see <http:--www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;


entity CPU_TB IS
end CPU_TB;

architecture behavior OF CPU_TB IS 

	--Inputs
	signal clk :	std_logic := '0';
	signal res :	std_logic := '0';
	signal nmi :	std_logic := '0';


begin

	CPU1: entity work.CPU
	generic map( "vhdl/iram.dat", "vhdl/dram.dat", 32, 10, 10, 4 ,4)
	port map( clk => clk,
	          res => res,
	          nmi => nmi,
	          PC_out => open,
	          io_ctrl_we => open,
	          io_ctrl_re => open,
	          io_alu_we => open,
	          io_alu_re => open,
	          io_addr => open,
	          io_ctrl_out => open,
	          io_ctrl_in => (others=>'0'),
	          io_ctrl_full  => '0',
	          io_ctrl_empty => '0',
	          io_alu_out => open,
	          io_alu_in => (others=>'0'),
	          io_alu_full  => '0',
	          io_alu_empty => '0'
	          );
	

	clock : process
	begin
		clk <= not clk;
		wait for 5 ns;
	end process;

	tb : process
	begin
		res <= '1';
		nmi <= '0';
		wait for 20 ns;
		res <= '0';
		wait; -- will wait forever
	end process;
end;
