#! /bin/sh




resfile="results.txt"
#resfile="/dev/tty"

echo ""


###############################################################
# S L I C E S
###############################################################

echo "#slices -------------------" 

#ergebnisse vertikal mit zeilenumbruch
i=8
while [ $i -le $1 ]
do
	cat ise/log/par$i.log | grep  "Number of Slices" \
  	| sed 's@   Number of Slices                        @@' \
  	| sed 's@ out of 13312   1%@@' \
  	| sed 's@ out of 13312   2%@@' \
  	| sed 's@ out of 13312   3%@@' \
  	| sed 's@ out of 13312   4%@@' \
  	| sed 's@ out of 13312   5%@@' \
  	| sed 's@ out of 13312   6%@@' \
  	| sed 's@ out of 13312   7%@@' \
  	
  
  i=`expr $i + 1`  
done

echo "" 

#ergebnisse horizontal in einer zeile
i=8
while [ $i -le $1 ]
do
	cat ise/log/par$i.log | grep  "Number of Slices" \
  	| sed 's@   Number of Slices                        @@' \
  	| sed 's@ out of 13312   1%@@' \
  	| sed 's@ out of 13312   2%@@' \
  	| sed 's@ out of 13312   3%@@' \
  	| sed 's@ out of 13312   4%@@' \
  	| sed 's@ out of 13312   5%@@' \
  	| sed 's@ out of 13312   6%@@' \
  	| sed 's@ out of 13312   7%@@' \
  	| tr "\n" " " \
  	
  
  i=`expr $i + 1`  
done



echo "" 

###############################################################
# D E L A Y
###############################################################


echo "" 
echo "#delay ------------------"   	


#ergebnisse vertikal mit zeilenumbruch
i=8
while [ $i -le $1 ]
do
	cat ise/log/par$i.log | grep  "Autotimespec constraint for clock net clk" \
  	| sed "s@  Autotimespec constraint for clock net clk | SETUP   |         N/A|    @@" \
  	| sed 's@ns|     N/A|           0@@' \
  	
  
  i=`expr $i + 1`  
done


echo "" 

#ergebnisse horizontal in einer zeile
i=8
while [ $i -le $1 ]
do
	cat ise/log/par$i.log | grep  "Autotimespec constraint for clock net clk" \
  	| sed "s@  Autotimespec constraint for clock net clk | SETUP   |         N/A|    @@" \
  	| sed 's@ns|     N/A|           0@@' \
  	| tr "\n" " " \
  	
  
  i=`expr $i + 1`  
done


echo "" 
echo "" 




