
/* TODO */
//kompatibel zu CPU rev 40

.section .defs
	//di_com_addr = 0x0000;
	//mmr_addr    = 0x0001;
	
	sinusSamples = 512;
	sinusSamples_4 = 128;
	logSinusSamples = 9;
	
	sampleCount = 128; // <----
	
	
	


.section .dmem

 pN_WAVE_4    : .dword sinusSamples_4;
 pN           : .dword sampleCount;
	pN_WAVE      : .dword sinusSamples;
	pLOG2_N_WAVE : .dword logSinusSamples;
	dStack       : .dword 0;
	isave        : .dword 0;
	lsave        : .dword 0;
	
	pSinewave    : .dword 	
 0,        402,        804,      1206,   
   1607,      2009,      2410,      2811,   
   3211,      3611,      4011,      4409,   
   4807,      5205,      5601,      5997,   
   6392,      6786,      7179,      7571,   
   7961,      8351,      8739,      9126,   
   9511,      9895,    10278,    10659,  
  11038,    11416,    11792,    12166,  
  12539,    12909,    13278,    13645,  
  14009,    14372,    14732,    15090,  
  15446,    15799,    16150,    16499,  
  16845,    17189,    17530,    17868,  
  18204,    18537,    18867,    19194,  
  19519,    19840,    20159,    20474,  
  20787,    21096,    21402,    21705,  
  22004,    22301,    22594,    22883,  
  23169,    23452,    23731,    24006,  
  24278,    24546,    24811,    25072,  
  25329,    25582,    25831,    26077,  
  26318,    26556,    26789,    27019,  
  27244,    27466,    27683,    27896,  
  28105,    28309,    28510,    28706,  
  28897,    29085,    29268,    29446,  
  29621,    29790,    29955,    30116,  
  30272,    30424,    30571,    30713,  
  
  30851,    30984,  
  31113,    31236,  
  
  31356,    31470,   31580,   31684, 
  31785,    31880,   31970,   32056, 
  32137,    32213,   32284,   32350, 
  32412,    32468,   32520,   32567, 
  32609,    32646,   32678,   32705, 
  32727,    32744,   32757,   32764, 
  32767,    32764,   32757,   32744, 
  32727,    32705,   32678,   32646, 
  32609,    32567,   32520,   32468, 
  32412,    32350,   32284,   32213, 
  32137,    32056,   31970,   31880, 
  31785,    31684,   31580,   31470, 
  31356,    31236,   31113,   30984, 
  30851,    30713,   30571,   30424, 
  30272,    30116,   29955,   29790, 
  29621,    29446,   29268,   29085, 
  28897,    28706,   28510,   28309, 
  28105,    27896,   27683,   27466, 
  27244,    27019,   26789,   26556, 
  26318,    26077,   25831,   25582, 
  25329,    25072,   24811,   24546, 
  24278,    24006,   23731,   23452, 
  23169,    22883,   22594,   22301, 
  22004,    21705,   21402,   21096, 
  20787,    20474,   20159,   19840, 
  19519,    19194,   18867,   18537, 
  18204,    17868,   17530,   17189, 
  16845,    16499,   16150,   15799, 
  15446,    15090,   14732,   14372, 
  14009,    13645,   13278,   12909, 
  12539,    12166,   11792,   11416, 
  11038,    10659,   10278,    9895,  
   9511,      9126,     8739,     8351,  
   7961,      7571,     7179,     6786,  
   6392,      5997,     5601,     5205,  
   4807,      4409,     4011,     3611,  
   3211,      2811,     2410,     2009,  
   1607,      1206,      804,       402,   
      0,      -402,      -804,    -1206,  
  -1607,    -2009,    -2410,    -2811,  
  -3211,    -3611,    -4011,    -4409,  
  -4807,   -5205,    -5601,    -5997,  
  -6392,   -6786,    -7179,    -7571,  
  -7961,   -8351,    -8739,    -9126,  
  -9511,   -9895,  -10278,  -10659, 
 -11038, -11416,  -11792,  -12166, 
 -12539, -12909,  -13278,  -13645, 
 -14009, -14372,  -14732,  -15090, 
 -15446, -15799,  -16150,  -16499, 
 -16845, -17189,  -17530,  -17868, 
 -18204, -18537,  -18867,  -19194, 
 -19519, -19840,  -20159,  -20474, 
 -20787, -21096,  -21402,  -21705, 
 -22004, -22301,  -22594,  -22883, 
 -23169, -23452,  -23731,  -24006, 
 -24278, -24546,  -24811,  -25072, 
 -25329, -25582,  -25831,  -26077, 
 -26318, -26556,  -26789,  -27019, 
 -27244, -27466,  -27683,  -27896, 
 -28105, -28309,  -28510,  -28706, 
 -28897, -29085,  -29268,  -29446, 
 -29621, -29790,  -29955,  -30116, 
 -30272, -30424,  -30571,  -30713, 
 -30851, -30984,  -31113,  -31236, 
 -31356, -31470,  -31580,  -31684, 
 -31785, -31880,  -31970,  -32056, 
 -32137, -32213,  -32284,  -32350, 
 -32412, -32468,  -32520,  -32567, 
 -32609, -32646,  -32678,  -32705, 
 -32727, -32744,  -32757,  -32764, 
 -32767, -32764,  -32757,  -32744, 
 -32727, -32705,  -32678,  -32646, 
 -32609, -32567,  -32520,  -32468, 
 -32412,  -32350,  -32284,  -32213, 
 -32137,  -32056,  -31970,  -31880, 
 -31785,  -31684,  -31580,  -31470, 
 -31356,  -31236,  -31113,  -30984, 
 -30851,  -30713,  -30571,  -30424, 
 -30272,  -30116,  -29955,  -29790, 
 -29621,  -29446,  -29268,  -29085, 
 -28897,  -28706,  -28510,  -28309, 
 -28105,  -27896,  -27683,  -27466, 
 -27244,  -27019,  -26789,  -26556, 
 -26318,  -26077,  -25831,  -25582, 
 -25329,  -25072,  -24811,  -24546, 
 -24278,  -24006,  -23731,  -23452, 
 -23169,  -22883,  -22594,  -22301, 
 -22004,  -21705,  -21402,  -21096, 
 -20787,  -20474,  -20159,  -19840, 
 -19519,  -19194,  -18867,  -18537, 
 -18204,  -17868,  -17530,  -17189, 
 -16845,  -16499,  -16150,  -15799, 
 -15446,  -15090,  -14732,  -14372, 
 -14009,  -13645,  -13278,  -12909, 
 -12539,  -12166,  -11792,  -11416, 
 -11038,  -10659,  -10278,   -9895,  
  -9511,    -9126,    -8739,    -8351,  
  -7961,    -7571,    -7179,    -6786,  
  -6392,    -5997,    -5601,    -5205,  
  -4807,    -4409,    -4011,    -3611,  
  -3211,    -2811,    -2410,    -2009,  
  -1607,    -1206,     -804,      -402;
	
	

	pRData      : .dword 1000,2000,3000,4000,5000,6000,7000,8000;
	pIData      : .dword 1000,2000,3000,4000,5000,6000,7000,8000;

	
	


.main: 


// decimation in time - re-order data
	i7.l=(dStack).l; //! decimation
	i7.h=(dStack).h;
	

//	i1.l=(di_com_addr).l;
//	i1.h=(di_com_addr).h;

	d0 = dmem(pN); //n
	dnop;
	d1 = 1;
	
	
	d2 = 0; //mr=0
	d4 = d0-d1; //n-1
	
	d3 = 1; //m=1
	
dec_begin:


	f=(d3>d0);
	cjf dec_end;
	inop;
	
	d5=d0; //l=n
	
	


inner_dec_loop:
	
	d5=d5>>1; //l >>= 1
	d6=d2+d5; //mr+l
	
	f=(d6>d4);
	cjf inner_dec_loop;
	inop;



	
	
	d5=d5-d1; // l=l-1
	d2=d2&d5; // mr=mr&(l-1);
	d2=d2+d5; // mr=( mr&(l-1) ) + (l-1);
	d2=d2+d1; // mr = (mr & (l-1)) + l;
	
	
	f=(d3<d2); //mr<=m
	cjf weiter;	
	dnop;
	jmp dec_begin;
	d3=d3+d1; //m++
weiter:
	
	//ab d5 frei
	
//	dmem(i1-i1)=d3; //m in imem port
//	i2=imem(i1); //i2 = m aus dmem port
//	dnop;
i2=d3;
	
//	dmem(i1-i1)=d2; //mr in imem port
//	//dnop;
//	i3=imem(i1); //i3 = mr aus dmem port
//	inop;
i3=d2;


	//////////////////////
	
	i0.l = (pRData).l; //! fr_adresse
	i0.h = (pRData).h;
	//i4=i0-i2;  //m index für r  (i2)
	//i5=i0-i3;  //mr index für r (i3)
	

	d5 = dmem(i0-i2); //tr = fr[m];

	d6 = dmem(i0-i3); //fr[m] = fr[mr];
	inop;
	dmem(i0-i2)=d6;
	
	dmem(i0-i3) = d5; //fr[mr] = tr;



	////////////////////
	i0.l = (pIData).l; //! i_index
	i0.h = (pIData).h;	
	//i4=i0-i2; //m  index für i
	//i5=i0-i3; //mr index für i
	
	
	d5 = dmem(i0-i2); //ti = fi[m];
	
	d6 = dmem(i0-i3); //fi[m] = fi[mr];
	inop;
	dmem(i0-i2)=d6;
	
	dmem(i0-i3)=d5;  //fi[mr] = ti;
	
		
	//////////////////


	jmp dec_begin;
	d3=d3+d1; //m++
	
dec_end:
	

	
	
	//jmp dec_end;
	dnop; 
	inop;
	dnop;
	inop;
	dnop;
	inop; //! fft





// fft ///////////////////////////////
	

	d0=dmem(pN); //N
	d1 = 1; 

	d2 = 1; //l=1
	
	d3 = dmem(pLOG2_N_WAVE);
	dnop;
	d3=d3-d1; //k = LOG2_N_WAVE-1
	
	

loop3_begin: //while(l < n)
	
	f=(d2>=d0); //! loop3_begin
	cjf loop3_end;

	inop;

	d4=d2<<1; //istep = l << 1
	
	
	
	d5=0; //m=0
loop2_begin: //for(m=0; m<l; ++m)
	f=(d5>=d2); //! loop2_begin
	cjf loop2_end;
	inop;
	
//	dmem(i1-i1)=d3;  //k in ctrl
	i0.l=1;
	i0.h=0;
//	i2=imem(i1); //k
//	inop;
i2=d3;
	
	i0=i0-i2; //schleifenzähler für shift


	f=(d3==0); //k==0

	
	cjf no_left_shift;
	d6=d5; //j=m	
	
left_shift:
	ijnz left_shift;
	d6=d6<<1; //! j_shiften
	// j = m << k;
	
	
no_left_shift:
	
	
	//j von alu in ctrl 
	//dmem(i1-i1)=d6; 
	//dnop;
	//i2 = imem(i1); //!j_in_ctrl
	i2=d6;
	
	i3.l = (pSinewave).l; 
	i3.h = (pSinewave).h;
	//i3 = imem(i3);
	//inop;
	//i4=i3-i2; //index j nach speicheradresse konvert.
	
	d8=dmem(i3-i2);
	dnop;
	d8=-d8;  //wi
	
	
	i3.l = (pN_WAVE_4).l;
	i3.h = (pN_WAVE_4).h;
	i3 = imem(i3);
	inop;
	//i4=i4-i3; //j+N_WAVE/4
	
	d7=dmem(i4-i3); //wr
	dnop;
	d7=d7>>1; //!wr
	d8=d8>>1; //!wi
	
	
	
	
	i4.l = (pRData).l;
	i4.h = (pRData).h;
	i5.l = (pIData).l;
	i5.h = (pIData).h;
	

	d9=d5;//i=m
loop1_begin: //for(i=m; i<n; i+=istep)
	f=(d9>=d0); //! loop1_begin
	cjf loop1_end;
	inop;
	
	
	d6=d9+d2; //j = i + l;
	
	
	//j und i in ctrl
	//dmem(i1-i1)=d6; //j_in_ctrl__
	//dnop;
	//i2 = imem(i1);
	//inop;
	i2=d6;
	
	//dmem(i1-i1)=d9; //i_in_ctrl__
	//dnop;
	//i3 = imem(i1);
	//inop;
	i3=d9;
	

	
	//i6=i4-i2; //zeiger auf fr[j]
	//i7=i5-i2; //zeiger auf fi[j]

	dmem(isave) = d9; //i speichern
	
	
	d9 =dmem(i4-i2); //fr[j]
	d10=dmem(i5-i2); //fi[j]
	dnop; 
	d11=(d7*d9).h;   // fix_mpy(wr,fr[j])
	d12=(d8*d10).h;  // fix_mpy(wi,fi[j])
	d11=d11-d12; //!tr

	d12=(d7*d10).h;  // fix_mpy(wr,fi[j])
	d13=(d8*d9).h;   // fix_mpy(wi,fr[j])
	d12=d12+d13; //!ti
	
	d9=dmem(isave); //i wiederherstellen
	dnop;

	
	//i4=i4-i3; //zeiger auf fr[i]
	//i5=i5-i3; //zeiger auf fi[i]
	
	d13=dmem(i4-i3); //qr = fr[i];
	d14=dmem(i5-i3); //qi = fi[i];
	dnop;
	
	d13=d13>>1; //!qr // qr >>= 1;
	d14=d14>>1; //!qi // qi >>= 1;
	
	

	d15=d13-d11;  //!fr_j
	dmem(i4-i2)=d15; //!n // fr[j] = qr - tr;
	//dnop;
	
	d15=d14-d12;  //!fi_j
	dmem(i5-i2)=d15; //!n // fi[j] = qi - ti;
	//dnop;
	
	d15=d13+d11;  //!fr_i
	dmem(i4-i3)=d15; //!n // fr[i] = qr + tr;
	//dnop;
	
	d15=d14+d12;  //!fi_j
	dmem(i5-i3)=d15; //!n // fi[i] = qi + ti;



	jmp loop1_begin;
	d9=d9+d4; //i+=istep
loop1_end:










	jmp loop2_begin;
	d5=d5+d1; //m++
loop2_end:




	d3 = d3 - d1; //k--
	d2 = d4;      //! l_eq_istep
	//l=istep

	
	jmp loop3_begin;
	dnop;
loop3_end:




fft_end:
	jmp fft_end;
	dnop;


/*


int fix_fft(fixed fr[], fixed fi[], int m, int inverse)
{
        int mr,nn,i,j,l,k,istep, n, scale, shift;
        fixed qr,qi,tr,ti,wr,wi,t;

                n = 1<<m;

        if(n > N_WAVE)
                return -1;

        mr = 0;
        nn = n - 1;
        scale = 0;

        // decimation in time - re-order data 
        for(m=1; m<=nn; ++m) {
                l = n;
                do {
                        l >>= 1;
                } while(mr+l > nn);
                mr = (mr & (l-1)) + l;

                if(mr <= m) continue;
                tr = fr[m];
                fr[m] = fr[mr];
                fr[mr] = tr;
                ti = fi[m];
                fi[m] = fi[mr];
                fi[mr] = ti;
        }

        l = 1;
        k = LOG2_N_WAVE-1;
        while(l < n) {
                if(inverse) {
                        // variable scaling, depending upon data 
                        shift = 0;
                        for(i=0; i<n; ++i) {
                                j = fr[i];
                                if(j < 0)
                                        j = -j;
                                m = fi[i];
                                if(m < 0)
                                        m = -m;
                                if(j > 16383 || m > 16383) {
                                        shift = 1;
                                        break;
                                }
                        }
                        if(shift)
                                ++scale;
                } else {
                        // fixed scaling, for proper normalization -
                        // there will be log2(n) passes, so this
                        // results in an overall factor of 1/n,
                        // distributed to maximize arithmetic accuracy. 
                        shift = 1;
                }
                // it may not be obvious, but the shift will be performed
                // on each data point exactly once, during this pass. 
                istep = l << 1;
                for(m=0; m<l; ++m) {
                        j = m << k;
                        // 0 <= j < N_WAVE/2 
                        wr =  Sinewave[j+N_WAVE/4];
                        wi = -Sinewave[j];
                        if(inverse)
                                wi = -wi;
                        if(shift) {
                                wr >>= 1;
                                wi >>= 1;
                        }
                        for(i=m; i<n; i+=istep) {
                                j = i + l;
                                        tr = fix_mpy(wr,fr[j]) - fix_mpy(wi,fi[j]);
                                        ti = fix_mpy(wr,fi[j]) + fix_mpy(wi,fr[j]);
                                qr = fr[i];
                                qi = fi[i];
                                if(shift) {
                                        qr >>= 1;
                                        qi >>= 1;
                                }
                                fr[j] = qr - tr;
                                fi[j] = qi - ti;
                                fr[i] = qr + tr;
                                fi[i] = qi + ti;
                        }
                }
                --k;
                l = istep;
        }

        return scale;
}


*/
