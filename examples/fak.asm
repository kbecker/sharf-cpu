/* long fakultaet(int n){
   if (n==0||n==1)
        return 1;
   else
        return n*fakultaet(n-1);
}
*/

//anfangswert in d0
//ergebnis in d0


.section .defs
         n = 7;
         //MAX_IWORD = 0x3FFFF;

.section .imem
         stack:   .iword 0;
         //one:     .iword 1;

.main:
          d0 = n;
          d2 = 0;
          d4 = 0;
          i7.l=0x1ff;
          i7.h=0x1ff;
          i2.l = 1;
          i2.h = 0;
          i3.l = (stack).l;
          i3.h = (stack).h;
fak:
          f=(d0==d4);
          cjf cond1;            // jump if n = 0;
          d1 = 1;
          d2 = d0;
          d0 = d0 - d1;
          dnop;
          f=(d0==d4);           // jump if n = 1;
          cjf cond1;
          inop;
          dmem(i3--) = d2;     // save n;
          inop;
          call fak;
          dnop;
          d1 = dmem(i3++);
          inop;
          d0 = d0 * d1;
          rts;
          inop;

cond1:
			       i3 = i3 - i7; //stackpointer +1
          d0 = 1;
          rts;
          inop;




