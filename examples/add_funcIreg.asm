/* long fakultaet(int n){
   if (n==0||n==1)
        return 1;
   else
        return n*fakultaet(n-1);
}
*/

#include "def.h"

.main:

	d0 = 1;

	_initspi;
	
	
	_assign(i1,1);
	_assign(i2,2);
	
	i2 = i2 + i1; // i2=3

	// übergabeparameter, am anfang	
	imem(spi--) = i2;  inop;// (1023)	= 3	
	// rückgabeparameter
	imem(spi--) = i0; inop; // (1022) = xxx / 0

	call _acc; inop;
	
	i0 = imem(spi++); inop;// abholen des werten vom stack	
	i2=i2-i1; //i2=2	
	i0=i2+i0; // i2 = 2 + 4

	jmp _end; inop;

/*************/
_acc:
	// [--sp] = rets 	
	imem(spi--) = rets; inop; // (1021) = i7 return addresse abspeichern
	imem(spi--) = i1;   inop; // (1020) = 1; ermitteln welche noch lebendig sind. diese dann abspeichern.
	imem(spi) = i2; inop; // (1019) = 3
	
	i1 = imem(spi+4); // i1 = (1023) = 3 <- stackpointer kennt die orte über- und rückgabe parameter.
	
	_assign(i2,1);	
	i1 = i1 + i2; // i1 = 1 + 3 = 4
	
	imem(spi+3) = i1; inop; // (1022) = 4, rückgabeparameter i0

	i2=imem(spi++); inop; // i2 wiederherstellen (1019) i2 = 3
	i1=imem(spi++); inop; // i1 wiederherstellen (1018) i1 = 1;
	rets=imem(spi++); inop;

	rts; inop;// der greift auf i7 zurück
	
/*************/	
_end:


