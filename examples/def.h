/*

Header Datei für die Benutzung des Assemblers.

Identischer Aufbau und Abarbeitung des Stacks für beide Speicher (Imem + Dmem), mit der Ausnahme dass die Rücksprungadresse im Imem zusätzlich abgespeichert werden muss.

### Frame (i-1) ###
# Eingehende Argumente aus Sicht des Frame i
arg n
...
arg 1
### Frame (i) ###
# nur beim Imem Rücksprungadresse
rets
# Abspeicherung von Registern vor dem nächsten Aufruf
i1
..
in
#Ausgehende Argumente aus Sicht des Frame i
arg n
...
arg 1
#### Frame (i+1) ###
...

Frame i gibt über die eingehenden Argumente seine Ergebnisse wieder.


*/
#define spd i13  /* Stack Pointer für die Alu */
#define spi i14  /* Stack Pointer für die CPU */
#define rets i15 /* Abspeicherungsort des PCs bei einem Call*/


/* Zuweisung in einer Zeile. */
#define _assign(x,y) \
x.l=(y).l;\
x.h=(y).h;

/* Inialisierung des Stackpointers. 
Achtung 0x3FF ist nur korrekt wenn der Cache mit 10 bit addressiert
also 1024 = 0x400 Wörter abspeichern kann. */
#define _initspi \
spi.l=(0x3FF).l;\
spi.h=(0x3FF).h;

/* Inialisierung des Stackpointers. Siehe _initspi */
#define _initspd \
spd.l=(0x3FF).l;\
spd.h=(0x3FF).h;

/* Wirft das oberste Elements des Stacks im Imem raus und packt
es ins definierte Indexregister x hinein. */
#define _ipop(x) \
spi++;\
x = imem(spi);//\
inop;

/* Gibt das oberste Elements des Stacks im Imem zurück und packt
es ins definierte Indexregister x hinein. */
#define _itop(x) \
x = imem(spi); //\
inop;

/* Wirft das oberste Elements des Stacks im Dmem raus und packt
es ins definierte Datenregister x hinein. */
#define _dpop(x) \
spd++;\
x = dmem(spd); \
inop;

/* Gibt das oberste Elements des Stacks im Imem zurück und packt
es ins definierte Indexregister x hinein. */
#define _dtop(x) \
x = dmem(spd); \
inop;

/* Legt das Datum im Indexregister x im Imem Stack ab*/
#define _ipush(x) \
imem(spi) = x; \
spi--; //\
inop;

/* Legt das Datum im Datenregister x im Dmem Stack ab. */
#define _dpush(x) \
dmem(spd) = x; \
spd--; \
dnop;

/* Hole die n-te Variable vom DStack ab. n beginnt bei 1.*/
#define _dgetVar(x,n) \
x = dmem(spd+n); \
inop;

/* Schreibe die n-te Variable in den DStack. */
#define _dsetVar(x,n) \
dmem(spd+n) = x; \
inop;

/* Hole die n-te Variable vom IStack ab. m beginnt bei 2=n+1 und
läuft bis m+1=n, da im Frame zuerst die Rücksprungadresse gespeichert ist.*/
#define _igetVar(x,m) \
x = imem(spi+m); //\
inop;

/* Schreibe die n-te Variable in den IStack. */
#define _isetVar(x,m) \
imem(spi+m) = x; //\
inop;

