////////////////////
/*********************************/
/***** Function test_schedule01 *************/
/*********************************/
.main:
LABEL_test_schedule01:
imem(i13+0) = i15;
i13 -= 1;

LABEL_test_schedule01_0:
d2 = 0;  // i = 0;
d1 = 0x3;  // T0 = 3;
d0 = 0x3;  // c = 3;
f = (d2 < d1); // i < T0
cjf LABEL_test_schedule01_2;
inop;

LABEL_test_schedule01_1:
d1 = d0; // c = c;
jmp LABEL_test_schedule01_5;
d0 = d2; // i = i;

LABEL_test_schedule01_2:
d1 = d0; // c = c;

LABEL_test_schedule01_3:
d0 = 0x1;  // T1 = 1;
d3 = d2 + d0; // i = i + T1; // d3 = i + T1;
d1 = d1 + d2; // c = c + i; // d1 = c + i;
d0 = 0x3;  // T2 = 3;
f = (d3 < d0); // i < T2
cjf LABEL_test_schedule01_6;
inop;

LABEL_test_schedule01_4:
d0 = d3; // i = i;

LABEL_test_schedule01_5:
jmp LABEL_test_schedule01_END;
d0 = d1 + d0; // ret0 = c + i; // d0 = c + i;

LABEL_test_schedule01_6:
jmp LABEL_test_schedule01_3;
d2 = d3; // i = i;

LABEL_test_schedule01_END:
i13 += 1;
i15 = imem(i13+0);
inop;
rts;
inop;

