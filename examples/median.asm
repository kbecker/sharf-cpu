/*TODO*/
//CPU rev 44

.section .defs
	sizex   = 16;//16;
	sizey   = 16;//16;
	sizex_1 = sizex - 1;
	sizex_3_neg = -(sizex - 3);
	
	di_comm = 0x0000;

.section .dmem       

         swap0:   .dword 0;
         swapi7:  .dword 0;
         swapi6:  .dword 0;
         loopy : .dword 1-sizey;
         loopx : .dword 1-sizex;
         loopy_swap: .dword 0;
         median_ret: .dword 0;
         median_ijnz: .dword 0;
         
         


	array_in:  .dword 
			103, 198, 105, 115, 81, 255, 74, 236, 41, 205, 186, 171, 242, 251, 227, 70, 
			124, 194, 84, 248, 27, 232, 231, 141, 118, 90, 46, 99, 51, 159, 201, 154, 
			102, 50, 13, 183, 49, 88, 163, 90, 37, 93, 5, 23, 88, 233, 94, 212, 
			171, 178, 205, 198, 155, 180, 84, 17, 14, 130, 116, 65, 33, 61, 220, 135, 
			112, 233, 62, 161, 65, 225, 252, 103, 62, 1, 126, 151, 234, 220, 107, 150, 
			143, 56, 92, 42, 236, 176, 59, 251, 50, 175, 60, 84, 236, 24, 219, 92, 
			2, 26, 254, 67, 251, 250, 170, 58, 251, 41, 209, 230, 5, 60, 124, 148, 
			117, 216, 190, 97, 137, 249, 92, 187, 168, 153, 15, 149, 177, 235, 241, 179, 
			5, 239, 247, 0, 233, 161, 58, 229, 202, 11, 203, 208, 72, 71, 100, 189, 
			31, 35, 30, 168, 28, 123, 100, 197, 20, 115, 90, 197, 94, 75, 121, 99, 
			59, 112, 100, 36, 17, 158, 9, 220, 170, 212, 172, 242, 27, 16, 175, 59, 
			51, 205, 227, 80, 72, 71, 21, 92, 187, 111, 34, 25, 186, 155, 125, 245, 
			11, 225, 26, 28, 127, 35, 248, 41, 248, 164, 27, 19, 181, 202, 78, 232, 
			152, 50, 56, 224, 121, 77, 61, 52, 188, 95, 78, 119, 250, 203, 108, 5, 
			172, 134, 33, 43, 170, 26, 85, 162, 190, 112, 181, 115, 59, 4, 92, 211, 
			54, 148, 179, 175, 226, 240, 228, 158, 79, 50, 21, 73, 253, 130, 78, 169;

	
	
	array_out: .dword
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;

	
	list: .dword 0,0,0,0,0,0,0,0,0;
	

	pStackD: .dword[20];

 pStackI:  .dword[20];
.main:



		//jmp isr;
dnop;		
dnop;

.start:	


	i0=imem(loopy); //schleifenzähler y
	d15=1;

	d1=1; //y=1
	
	
loopy_begin:


	imem(loopy_swap)=i0;

	i0=imem(loopx); //schleifenzähler x

	d2=1; //x=1

loopx_begin:
	
	d0=0;
	
	i2.l = (array_in).l;
	i2.h = (array_in).h;
	
	i3.l = (list).l;
	i3.h = (list).h;

	imem(median_ijnz)=i0;

	call .P_function; //! call_P_func
	dnop;


	call median; //! call_Median
	dnop;


	d4=sizex; //! d4_eq_size_x
	d4=d4*d1; //y;
	d4=d4+d2; //index von element x,y

 i4 = d4;

	i0=imem(median_ijnz);
	
 inop;
	
	d4=dmem(i3-4); //5. listenelement
	
	i6.l = (array_out).l;
	i6.h = (array_out).h;
	
	dmem(i6-i4)=d4;

	ijnz loopx_begin;
	d2=d2+d15; //x++	
	
	i0=imem(loopy_swap); //zähler von loopy wiederherstellen
 inop;

	ijnz loopy_begin;
	d1=d1+d15; //y++	

	d0=0;
	i0.l=1;
	i0.h=0;
	
	dmem(i0)=d0;
	d0=1;
	dmem(i0)=d0;



hhhh:
	jmp hhhh;
	dnop;

// ENDE ///////////////////////////////////////////////////

	d0=0;
	
	d1 = 7; //y
	d2 = 9; //x

	i2.l = (array_in).l;
	i2.h = (array_in).h;
	
	i3.l = (list).l;
	i3.h = (list).h;



	
	call .P_function; //! call_P_func
	dnop;
	




////////////////////////////////////////////////////////////////////////////////////////
median:

	imem(median_ret)=i7; //! start_Median //rücksprungadresse sichern

// innerste schleife ///////////////////////////
//     for (j=1;j<9;j++) {
//         i = j-1;
//         tmp = list[j];
//         while(i>=0 && list[i] > tmp){
//             list[i+1] = list[i];
//             i-- ;
//         }
//         list[i+1] = tmp;
//     }

	
	i7.l = (-1).l;
	i7.h = (-1).h;
	i6.l = 1;
	i6.h = 0;
	
	i0.l=(1-8).l; //schleifenzähler
	i0.h=(1-8).h;
	
	i1.l=1; //j listenindex
	i1.h=0;
	
	
	
	
loop2_begin:
	
	i2=i1-i6; //i=j-1 listenindex

 d4 = i2;

	d3 = dmem(i3-i1);
	dnop;
 
	
	d5=dmem(i3-i2); //list[i]

loop1_begin:
	//while(i>=0 && list[i] > tmp)
	f=(d4<d0);//i>=0
	cjf loop1_end;
	f=(d5<=d3); //list[i] > tmp
	cjf loop1_end;
	dnop;

	d5=dmem(i3-i2++);//list[i];
	dnop;
	dmem(i3-i2--)=d5;  //list[i+1]
	
	i2=i2-i6; //i-- (i index)
	

	d5=dmem(i3-i2); //list[i] aktualisieren für abfrage in schleife
	
	
	jmp loop1_begin; 
 d4 = i2;

	
	
loop1_end:
	
	i4=i3-i2; //i adresse neu berechnen
	
	dmem(i4-1)=d3; //list[i+1] = tmp;


	ijnz loop2_begin;
	i1=i1-i7; //j++ listenindex dekrementieren



	
	//aus median zurück
	i7=imem(median_ret); //rücksprungadresse wiederherstellen
 inop;	
 rts; //! rts_Median
	dnop;


////////////////////////////////////////////////////////////////////////////////////////



	
.end:
	jmp .end;
	dnop;












///////////////////////////////////////////////////////////


.P_function: //params   array_in:i2, y:d1, x:d2, list:i3

	//register sichern
	imem(swapi7) = i7; //! star_P_func
	inop;
	imem(swapi6) = i6;
	
	i7.l=(pStackD).l;
	i7.h=(pStackD).h;
	
	i6.l=(pStackI).l;
	i6.h=(pStackI).h;
	
	
	dmem(i7--)=d8;
	dmem(i7--)=d9;
	dmem(i7--)=d5;
	dmem(i7--)=d7;

	imem(i6--)=i0;
	d8 = 1;
	imem(i6--)=i4;
	d9=0;
	imem(i6--)=i5;

	d5=d2-d8; //x_1=x-1;
	d7=d1-d8; //y_1=y-1;

	d8=sizex;
	
	//list[0] = image[y  ][x  ];
	d9=d8*d7; //sizex*(y-1);
	d9=d9+d5; //sizex*(y-1)+(x-1)

 i4 = d9;
	
	i0.l=(sizex_3_neg).l; //größe vom array um drei verringert
	i0.h=(sizex_3_neg).h;

	

	i5=i3;

	d9 = dmem(i2-i4++);
	dnop;
	dmem(i5--) = d9;

	d9 = dmem(i2-i4++);
	dnop;
	dmem(i5--) = d9;	
	
	d9 = dmem(i2-i4++);
	dnop;
	dmem(i5--) = d9;
	
	i4 = i4-i0; //in nächste zeile, erste spalte springen
	



	d9 = dmem(i2-i4++);
	dnop;
	dmem(i5--) = d9;

	d9 = dmem(i2-i4++);
	dnop;
	dmem(i5--) = d9;	
	
	d9 = dmem(i2-i4++);
	dnop;
	dmem(i5--) = d9;
	
	i4 = i4-i0; //in nächste zeile, erste spalte springen
	
	
	
	d9 = dmem(i2-i4++);
	dnop;
	dmem(i5--) = d9;

	d9 = dmem(i2-i4++);
	dnop;
	dmem(i5--) = d9;	
	
	d9 = dmem(i2-i4++);
	dnop;
	dmem(i5--) = d9;
	





	//register wiederherstellen
	d7=dmem(i7++);
	d7=dmem(i7++); //2 mal, damit der stackindex stimmt
	d5=dmem(i7++);
	d9=dmem(i7++);
	d8=dmem(i7++);

	i5=imem(i6++);
 inop;
	i5=imem(i6++); //2 mal, damit der stackindex stimmt
 inop;	
 i4=imem(i6++);
 inop;	
 i0=imem(i6++);
 inop;

	i7=imem(swapi7);
 inop;
	i6=imem(swapi6);
 inop;

	rts; //! rts_P_func
	dnop;


////////////////////////////////////////////////////////////////////////////////////////






/*
void P(int image[][FCOL],int y, int x, int list[]) {
   int y1, y_1, x1, x_1;

   x1=x+1;
   x_1=x-1;
   y1=y+1;
   y_1=y-1;

   if (x_1<0) x_1=x_1+FCOL;
   if (y_1<0) y_1=y_1+FROW;
   if (x1>FCOL-1) x1=x1-FCOL;
   if (y1>FROW-1) y1=y1-FROW;

   list[0] = image[y  ][x  ];
   list[1] = image[y_1][x_1];
   list[2] = image[y  ][x_1];
   list[3] = image[y1 ][x_1];
   list[4] = image[y1 ][x  ];
   list[5] = image[y1 ][x1 ];
   list[6] = image[y  ][x1 ];
   list[7] = image[y_1][x1 ];
   list[8] = image[y_1][x  ];
   
}


void median(int image_in[][FCOL], int image_out[][FCOL]) {

   int x, y, tmp, i, j;
   int list[9];

   for(y=1;y<FROW-1;y++)
       for(x=1;x<FCOL-1;x++) {
           P(image_in,y,x,p);
           for (j=1;j<9;j++) {
               tmp = list[j];
               i = j-1;
               while(i>=0 && list[i] > tmp){
                   list[i+1] = list[i];
                   i-- ;
               }
               list[i+1] = tmp;
           }
             image_out[y][x]=list[4];
       }
}    


*/


