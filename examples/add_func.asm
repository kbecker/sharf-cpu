/* 
int acc(j)
{
	return j+1;
}


int main()
{
	i1 = 1;
	i2 = 2;
	
	i2 += i1; // 3
	i1 = acc(i2);
	
}

*/

#include "def.h"

.main:

	_initspi;
	_initspd;
	
	
	d1=1;
	d2=2;
	
	d2 = d2 + d1; // d2=3

	// übergabeparameter, am anfang	
	dmem(spd--) = d2;  inop;// d(1023)	= 3	

	call _acc; inop;
	
	// d0 = d(1022) = 4, abholen des werten vom stack	
	spd++;
	d0 = dmem(spd); inop;
	spd++;
	// stackpointer muss auf die untereste adresse zeigen.
	d2=d2-d1; //i2=2	
	d0=d2+d0; // i2 = 2 + 4

	
	
	jmp _end; inop;

/*************/
_acc:
	// [--sp] = rets 	
	// i(1023) = i7 return addresse abspeichern
	imem(spi--) = rets; inop; 
		// d(1021) = 1; welche var lebendig -> abspeichern
	dmem(spd--) = d1; inop; 
	// d(1020) = 3
	dmem(spd--) = d2; inop; 
	
	// d1 = d(1023) = 3 , übergabeparameter abholen
	d1 = dmem(spd+4); inop;
	
	d2 = 1;	
	d1 = d1 + d2; // i1 = 3 + 1 = 4
	
	// d(1022) = 4, rückgabeparameter d0 abspeichern
	dmem(spd+3) = d1; inop; 

// d2 wiederherstellen (1020) d2 = 3
	spd++;
	d2=dmem(spd); inop; 
	// d1 wiederherstellen (1021) d1 = 1;
	spd++;
	d1=dmem(spd); inop; 
	// rets vor dem springen wiederhertstellen 
	inop; inop; inop;
	spi++;
	rets=imem(spi); inop; 

	rts; inop;// der greift auf i7 zurück
	
/*************/	
_end:


