/*
long fib(int n){
	if (n==0)
		return 0;
	else if (n == 1)
		return 1;
	else
		return fib(n-1)*fib(n-2);
}
*/

#include "def.h"

#define n 8;

.main:

	_initspi;
	_initspd;
	
	d0=n;
	
	// übergabeparameter, rückgabeparameter = n
	_dpush(d0);
	
	call FIB; inop;
		
	_dpop(d0);
	
	d7 = 0;
	d7 = 5;
	
	jmp END; inop;
	
/*************/
FIB:

	// return addresse abspeichern
	_ipush(rets);

	d6 = 0;
	d6 = 1;

	// n abholen
	_dgetVar(d0,1);
	
	// if(n == 0);
	d1 = 0;	
	f = (d0 == d1);
	cjf RET_0; inop;
	
	// else if( n== 1);
	d1 = 1;
	f = (d0 == d1);
	cjf RET_1; inop;
	
	// else --------------	
	// d1 = n-1;
	d1 = d0 - d1;	
	
	//abspeicherung von n für fib(n-2);	
	_dpush(d0);
	
	//üergabe von n-1
	_dpush(d1);
	call FIB; inop;
	
	// rückgabe von fib(n-1);
	_dpop(d1);
	
	// wiederherstellung von n
	_dpop(d0);

	// d2 = n-2;
	d2 = 2;
	d2 = d0 - d2;
	
	// abspeicherung von fib(n-1)
	_dpush(d1);
	
	// übergabe von n-2
	_dpush(d2);	
	call FIB; inop;			
	
	// rückgabe von fib(n-2);
	_dpop(d2);
	
	// wiederherstellung von fib(n-1);
	_dpop(d1);
	
	// fib(n-1) * fib(n-2);
	d0 = d1 + d2; inop;
	
	d6 = 0;
	d6 = 2;

	// rückgabe parameter weitergeben an calling func.
	_dsetVar(d0,1);
	// rets vor dem springen wiederhertstellen 
	_ipop(rets);
	rts; inop;

RET_0:
	// rückgabe parameter weitergeben an calling func.
	d6 = 0;
	d6 = 3;
	
	d0 = 0;
	_dsetVar(d0,1);
	
	// rets vor dem springen wiederhertstellen 	
	_ipop(rets);	
	rts; inop;

RET_1:
	// rückgabe parameter weitergeben an calling func.
	d6 = 0;
	d6 = 4;
	
	d0 = 1;
	_dsetVar(d0,1);
	
	// rets vor dem springen wiederhertstellen 	
	_ipop(rets);	
	rts; inop;

/*************/	
	
END:


