/* long fakultaet(int n){
   if (n==0||n==1)
        return 1;
   else
        return n*fakultaet(n-1);
}
*/

#include "def.h"

#define n 5;

.main:

	_initspi;
	_initspd;
	
	d0=n;
	
	// übergabeparameter, rückgabeparameter = n
	_dpush(d0);
	
	call FAK; inop;
		
	_dpop(d0);
	
	d6 = 0;
	d6 = 4;
	d6 = 0;
	d6 = 4;
	
	jmp END; inop;
	
/*************/
FAK:

	// return addresse abspeichern
	_ipush(rets);

	d6 = 0;
	d6 = 1;
	// da man keinen framepointer hat, muss ein großer offset 
	// aushelfen, um den übergabeparameter zu laden.
	_dgetVar(d0,1);
	
	// if(n == 0);
	d1 = 0;	
	f = (d0 == d1);
	cjf RET_1; inop;
	
	// if( n== 1);
	d1 = 1;
	f = (d0 == d1);
	cjf RET_1; inop;
	
	// else --------------
	
	// d1 = n-1;
	d1 = d0 - d1;
	
	//abspeichern von n
	_dpush(d0);	
	
	// übergabe + rückgabe an SUB_FAK n-1
	_dpush(d1);
	
	call FAK; inop;			
	
	// rückgabe abholen von SUB_FAK
	// fak(n-1)
	_dpop(d1);
	
	// d0 = n, aus dem stack das vorherige ergebniss abholen.
	_dpop(d0);
	
	// n * fak(n-1);
	d0 = d0 * d1; inop;
	
	d6 = 0;
	d6 = 2;

	// rückgabe parameter weitergeben an calling func.
	_dsetVar(d0,1);
	// rets vor dem springen wiederhertstellen 
	_ipop(rets);
	rts; inop;

RET_1:
	// rückgabe parameter weitergeben an calling func.
	d6 = 0;
	d6 = 3;
	
	d0 = 1;
	_dsetVar(d0,1);
	
	// rets vor dem springen wiederhertstellen 	
	_ipop(rets);	
	rts; inop;

/*************/	
	
END:


