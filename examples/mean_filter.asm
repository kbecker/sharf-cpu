/*
#define POS (i*N+j)


for(int i=0; i < M; i++)
{
  for(int j=0; j < N; j++)
     O[POS] = mean(&I[POS]);
}

int mean(int* img)
{
  int tmp = 0;
  for(int i = -fil_size/2; i < fil_size/2; i++)
  {
    for(int j = -fil_size; j < fil_size/2; j++)
    {
      tmp += img[POS]
    }
  }
  return (int)(tmp/(fil_size*fil_size));
}
*/


.section .defs
         n = 10;
         m = 10;

.section .dmem
	Input :   .dword 
                    0,  0,  0,  0,  0, 255, 255, 255, 255, 255,
                    0,  0,  0,  0,  0, 255, 255, 255, 255, 255,
                    0,  0,  0,  0,  0, 255, 255, 255, 255, 255, 
                    0,  0,  0,  0,  0, 255, 255, 255, 255, 255, 
                    0,  0,  0,  0,  0, 255, 255, 255, 255, 255, 
                    0,  0,  0,  0,  0, 255, 255, 255, 255, 255, 
                    0,  0,  0,  0,  0, 255, 255, 255, 255, 255, 
                    0,  0,  0,  0,  0, 255, 255, 255, 255, 255, 
                    0,  0,  0,  0,  0, 255, 255, 255, 255, 255, 
                    0,  0,  0,  0,  0, 255, 255, 255, 255, 255;

 Output : .dword
                    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
                    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
                    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
                    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
                    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
                    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
                    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
                    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
                    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
                    0,  0,  0,  0,  0,  0,  0,  0,  0,  0;

// 0 0 0 0 0 0 0 0 0 0
// 0 0 0 0 85 170 255 255 255 0
// 0 0 0 0 85 170 255 255 255 0
// 0 0 0 0 85 170 255 255 255 0
// 0 0 0 0 85 170 255 255 255 0
// 0 0 0 0 85 170 255 255 255 0
// 0 0 0 0 85 170 255 255 255 0
// 0 0 0 0 85 170 255 255 255 0
// 0 0 0 0 85 170 255 255 255 0
// 0 0 0 0 0 0 0 0 0 0




.main:

  i1.l = (Input).l;
  i1.h = (Input).h;
  i2.l = (Output).l;
  i2.h = (Output).h;
  
  i3.l = (0x1 - n).l;
  i3.h = (0x1 - n).h;

  i4.l = (0x1 - m).l;
  i4.h = (0x1 - m).h;

outer: 

  i2 = i4;
  acc  = 0x0;
  // innerloop counter herstellen
  i0   = i5; 

// berchne c[j]
inner: 
  d2 = dmem(i1--);
  d3 = dmem(i2--);
  ijnz inner;
  acc += d2 * d3;
  
  // outerloop counter herstellen
  i0 = i6;  
  // c[j] abspeichern
  d0 = acc.l;
  dmem(i3--) = d0;
  d1 = acc.h;  
  dmem(i3--) = d1; 

  ijnz outer;
  i6 = i0; // outerloop counter sichern
