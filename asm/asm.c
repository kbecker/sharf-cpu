//Copyright (C) 2013 Cem Bassoy.
//
//This file is part of the Sharf CPU.
//
//Sharf CPU is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Sharf CPU is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Sharf CPU.  If not, see <http:--www.gnu.org/licenses/>.

/*

63 +
62 -

Changelog:
	erkennt multiple Definitionen von Labels
	fehler bei bb!=0 behoben
	labels vom rest getrennt
	section
	i2 = label;
	imem(label)=i6;
	i2 = imem(label);
	dmem(label)=d6;
	d2 = dmem(label);
	label+-offset;
	&label+-offset !!!entfernt!!!
	
	-l druckt labels aus
	-d datei disassembler
	-s imemsize dmemsize adressbreite imem, dmem
	arithmetische ausdrücke statt zahlen
	d/imemsize im parameter angeben
	
	section const statt section defs
	.l und .h für konstanten
	
TODO
	warnungen ausschalten
	makros
	überläufe prüfen (struct, mem, ...)
	// kommentar im \* *\ kommentar ergibt syntaxfehler
	


  (#define)
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "asm.h"	
#include "parser.h"
#include "scanner.c"

#include "util.h"
#include "dasm.h"

struct Properties properties;


int imemSize = 1024;/*16384;  2^14 */
int dmemSize = 1024;  /* 2^10 */

int showWarnings = 0;


int opcode[1024];
int opcodeIndex = 0;

long long dmem[1024];
int dmemIndex = 0;

int imem[1024];
int imemIndex = 0;



	
	
struct LabelTab labelDefs[1000];
int labelDefIndex = 0;

struct LabelTab labelCmds[1000];
int labelCmdIndex = 0;

struct LabelTab dLabelDefs[1000];
int dLabelDefIndex = 0;

struct LabelTab iLabelDefs[1000];
int iLabelDefIndex = 0;

struct ConstTab constTab[1000];
int constTabIndex = 0;


int findLabel(const struct LabelTab *labelDefs, const char *label, int count)
{
	while(count-->=0) {
		if(strcmp( (labelDefs+count)->name, label)==0 )
			return count;
	}
	return -1;
}

void warning(const char *str)
{
	if(showWarnings==0) return;
	fprintf(stderr, "%i: Warning: %s",yylineno,str );
}
	





int writeDMEM(const char *fileName, const int ascii, const int word_len)
{
	int i;
	char str[word_len+1];
	FILE *dmemFile = (ascii==1)? fopen(fileName,"w") : fopen(fileName,"wb");
	if (!dmemFile) return -1;
	
	if(ascii==1)
	{
		//zero
		int2bin(0,str, word_len);
		for(i = 0; i < dmemSize-dmemIndex; i++)
			fprintf(dmemFile, "%s\n",str);
		
		//data
		for(i=dmemIndex-1; i>=0; i--) {
			int2bin(dmem[i], str, word_len);
			fprintf(dmemFile, "%s\n",str);
		}
	}
	else
	{
		//zero
		long long zero = 0;
		for(i = 0; i < dmemSize-dmemIndex; i++)
			fwrite(&zero,sizeof(zero),1,dmemFile);
		
		//data
		for(i=dmemIndex-1; i>=0; i--) 
			fwrite(&dmem[i],sizeof(dmem[i]),1,dmemFile);
	}
	fclose(dmemFile);

	return 0;
}

int writeIMEM(const char *fileName, unsigned int ascii, unsigned int codeOffset)
{
	int i, firstIndex=0;
	char str[19];
	FILE *imemFile = (ascii==1)? fopen(fileName,"w") : fopen(fileName,"wb");
	if (!imemFile) return -1;

	if(ascii==1)
	{
		//if(codeOffset!=0) {
		//	int2bin(codeOffset, str); //'jmp codeoffset' command
		//	fprintf(imemFile, "%s\n",str);
		//	firstIndex=1;
		//}
	
		//code offset
		for(i=firstIndex; i<codeOffset; i++)
			 fprintf(imemFile, "000000000000000000\n");
			 			
		//instructions
		for(i=0; i<opcodeIndex; i++) {
			int2bin(opcode[i], str,18);
			fprintf(imemFile, "%s\n",str);	
		}

		//zero
		for(i=0; i<imemSize-opcodeIndex-imemIndex-codeOffset; i++)
			 fprintf(imemFile, "000000000000000000\n");

		//data
		for(i=imemIndex-1; i>=0; i--) {
			int2bin(imem[i], str,18);
			fprintf(imemFile, "%s\n",str);
		}
	}
	else
	{
		unsigned int zero = 0;
		
		//if(codeOffset!=0) {
		//	fwrite(&codeOffset,sizeof(zero),1,imemFile); //'jmp codeoffset' command
		//	firstIndex = 1;
		//}
		
		//code offset
		for(i=firstIndex; i<codeOffset; i++)
			fwrite(&zero,sizeof(zero),1,imemFile);
		
		//instructions
		for(i=0; i<opcodeIndex; i++) 
			fwrite(&opcode[i],sizeof(opcode[i]),1,imemFile);

		//zero
		for(i=0; i<imemSize-opcodeIndex-imemIndex-codeOffset; i++)
			 fwrite(&zero,sizeof(zero),1,imemFile);

		//data
		for(i=imemIndex-1; i>=0; i--) 
			fwrite(&imem[i],sizeof(imem[i]),1,imemFile);
	}
	
	fclose(imemFile);
	
	return 0;
}


int yyerror(char *s)
{
	fprintf(stderr,"%i: %s\n", yylineno, s);
	return 0;
}

int main(int argc, char **argv)
{
	int error = 0;
	int i;
	char *inFileName = 0;
	char *outFileName1 = 0, *outFileName2 = 0;
	char *imemdat = "imem.dat";
	char *dmemdat = "dmem.dat";
	int printLabels = 0;
	int ascii = 0;
	int word_len = 18;
	//int codeOffset = 0x20;
	int disasm = 0;

	properties.codeOffset=0x20;
	properties.addressLabelFile[0]=0;

	for(i=1; i<argc; i++)
	{
		if(strcmp(argv[i],"-v")==0) {
			printf("CPU1C Assembler\nVersion 0.7 beta\n");
			if(i==1) return 0;
		}
		if(strcmp(argv[i],"-Wall")==0) {
			showWarnings = 1;
		}
		else if(strcmp(argv[i],"-o")==0) {
			if(argc>i+1)
				outFileName1 = argv[++i];
			if(argc>i+1)
				outFileName2 = argv[++i];

		}
		else if(strcmp(argv[i],"-d")==0) {
			disasm = 1;
		}
		else if(strcmp(argv[i],"-s")==0) {
			printLabels = 1;
		}
		else if( strcmp(argv[i],"-e")==0 || strcmp(argv[i],"--entry")==0 ) {
			if(argc>i+1) {
				i++;
				if(argv[i][0]=='0' && argv[i][1]=='x')
					properties.codeOffset = strtoul(&argv[i][2], 0, 16);
				else
					properties.codeOffset = atoi(argv[i]);
			}
		}
		else if(strcmp(argv[i],"-w")==0) {
			if(argc>i+1)
				word_len = atoi(argv[++i]);
		}
		else if(strcmp(argv[i],"-mem")==0) {
			if(argc>i+1)
				imemSize = atoi(argv[++i]);
			if(argc>i+1)
				dmemSize = atoi(argv[++i]);
		}
		else if( strcmp(argv[i],"-l")==0 || strcmp(argv[i],"--label")==0) {
			if(argc>i+1) {
				strcpy(properties.addressLabelFile,argv[++i]);
				//nicht so schick, aber egal...
				FILE *file = fopen(properties.addressLabelFile,"w");
				fclose(file);
			}
		}
		else if(strcmp(argv[i],"--help")==0 || strcmp(argv[i],"-h")==0) {
			printf("Aufruf: %s file [options]\n",argv[0]);
			printf("Options:\n");
			printf("\t-o <iFile> <dFile>      Write imem/dmem output into <iFile> and <dFile>\n");
			printf("\t-s                      Print symbol table after assembling\n");
			printf("\t-mem <iSize> <dSize>    Set imem/dmem size (default 16384/1024 words)\n");
			printf("\t-h --help               Print this help\n");
			printf("\t-v                      Print version\n");
			printf("\t-d                      Disassemble imem file\n");
			printf("\t-w                      Word length of the dmem\n");
			printf("\t-Wall                   Show warnings\n");
			printf("\t-a --ascii              Create ASCII output files\n");
			printf("\t-e --entry              Set entry address of code (default 0x0)\n");
			printf("\t-l --label <file>       Write address labels to <file>\n");

			return 0;
		}
		else if(strcmp(argv[i],"--ascii")==0 || strcmp(argv[i],"-a")==0) {
			ascii = 1;
		}
		else {
			if (i==1) {
				inFileName = argv[1];
			}
			else {
				fprintf(stderr, "error: unknown option '%s'\n", argv[i]);
				return -1;
			}
		}
		
	}
	if (inFileName==0) {
		fprintf(stderr,"%s: no input file\n", argv[0]);
		return -1;
	}
	if(disasm) {
		disassemble(inFileName, properties.codeOffset);
		return 0;
	}
	if (outFileName1==0) outFileName1 = imemdat;
	if (outFileName2==0) outFileName2 = dmemdat;


	if(inFileName[0]=='s') 
		{}
	else
		yyin = fopen(inFileName, "r");
		
	if(!yyin)
	{
		fprintf(stderr,"%s: error opening <%s>\n", argv[0], inFileName);
		return -1;
	}


	//yydebug=1;
	
	error = yyparse();

	fclose(yyin);

	if(error!=0)
	{
		return -1;
	}
	
	//replace labels by addresses
	for(i=0; i<labelCmdIndex; i++)
	{
		int j = findLabel(labelDefs, labelCmds[i].name, labelDefIndex);
		if(j<0)
		{
			fprintf(stderr,"%i: ERROR: label <%s> not defined in instructions\n",labelCmds[i].line, labelCmds[i].name);
			return -1;
		}
		
		opcode[labelCmds[i].adr] |= instrIndex2Adr(labelDefs[j].adr,properties.codeOffset);
		if(opcode[labelCmds[i].adr]==((0x4<<14)+0x3fff))
			 warning("\"call 0x3fff\" equals \"rts\"\n");
	}


	writeIMEM(outFileName1, ascii, properties.codeOffset);
	writeDMEM(outFileName2, ascii, word_len);
	
	if(printLabels)
	{
		int i;
		if(constTabIndex>0) {
			printf("Definition of Constants:\n");
			for(i=0; i<constTabIndex; i++)
				printf("\t%s: val 0x%x\n",constTab[i].name, constTab[i].value);
		}		
		if(iLabelDefIndex>0) {
			printf("\nIMEM Label Definitions:\n");
			for(i=0; i<iLabelDefIndex; i++) 
				printf("\t%s: adr %i/0x%x, val %i\n",iLabelDefs[i].name, imemSize-iLabelDefs[i].adr-1, imemSize-iLabelDefs[i].adr-1, imem[iLabelDefs[i].adr]);
		}
		if(dLabelDefIndex>0) {
			printf("\nDMEM Label Definitions:\n");
			for(i=0; i<dLabelDefIndex; i++)
				printf("\t%s: adr %i/0x%x, val %lli\n",dLabelDefs[i].name, dmemSize-dLabelDefs[i].adr-1, dmemSize-dLabelDefs[i].adr-1, dmem[dLabelDefs[i].adr]);
		}
		if(labelDefIndex>0) {
			printf("\nINSTR Label Definitions:\n");
			for(i=0; i<labelDefIndex; i++)
				printf("\t%s: adr %i/0x%x\n",labelDefs[i].name, labelDefs[i].adr, labelDefs[i].adr);
		}
	}

	
	return 0;
}


