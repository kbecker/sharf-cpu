//Copyright (C) 2013 Cem Bassoy.
//
//This file is part of the Sharf CPU.
//
//Sharf CPU is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Sharf CPU is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Sharf CPU.  If not, see <http:--www.gnu.org/licenses/>.


typedef void(*TDAssembleFcn)(char*, const unsigned int);
TDAssembleFcn getDAssembleFcn(unsigned int mcode);
int disassemble(char *fileName, unsigned int codeOffset);

struct CPUCmd
{
	unsigned int cmdAssembleMask;
	unsigned int cmdAssembleMaskValid;
	void (*cmdDAssembleFcn)(char *, const unsigned int);
	unsigned int group;
};
