%option noyywrap
%option yylineno

DecNum    [0-9]+
HexNum    "0"[x|X]([0-9a-fA-F])+
Space     [ \t\n\r]
Spaces    {Space}*
 
%{
	#include <stdlib.h>
%}

%%

{Space} ;  
".section"      return SECTION;
".dmem"         return DMEM_SEC;
".imem"         return IMEM_SEC;
".dword"        return DWORD;
".iword"        return IWORD;
".main"         return MAIN;
".defs"         return DEF_SEC;

"acc.l"         return ACC_L;
"acc.h"         return ACC_H;
"acc"           return ACC;

").l"           return CLOSE_PAR_L;
").h"           return CLOSE_PAR_H;
"--"            return MINUS_2;
"++"            return PLUS_2;
"+="            return ADD_EQUAL;
"-="            return SUB_EQUAL;

"jmp"{Space}+   return JMP;
"call"{Space}+  return CALL;
"cjf"{Space}+   return CJ;
"ijnz"{Space}+  return IJNZ;
"rts"           return RTS;
"dnop"          return DNOP;
"inop"          return INOP;
"io"            return IO;
"res.l"         return RES_L;
"res.h"         return RES_H;
"res"           return RES;

{DecNum}        yylval.uNum=atoi(yytext);         return (DEC_NUM);
{HexNum}        yylval.uNum=strtol(yytext+2,0,16);return (HEX_NUM);

"="             return (*yytext);
";"             return (*yytext);
"+"             return (*yytext);
"-"             return (*yytext);
"*"             return (*yytext);
"("             return (*yytext);
")"             return (*yytext);
"]"             return (*yytext);
"["             return (*yytext);
"}"             return (*yytext);
"{"             return (*yytext);
":"             return (*yytext);
","             return (*yytext);
"&"             return (*yytext);
"|"             return (*yytext);
"#"             return (*yytext);
">"             return (*yytext);
"<"             return (*yytext);

"f"             return (*yytext);
"imem"          return IMEM;
"dmem"          return DMEM;

"=="            return EQUAL;
"!="            return NEQUAL;
">="            return GEQUAL;
"<="            return LEQUAL;

">>"            return SHIFT_RIGHT;
"<<"            return SHIFT_LEFT;



"d"[0-1]?[0-9]".h" { if(yytext[4]==0) 
                  { 
                  	yylval.uNum = yytext[1]-'0';
                  	return(DREG_HI); 
                  }
                  else
                  {
                  	yylval.uNum = 10*(yytext[1]-'0') + yytext[2]-'0';
                  	if(yylval.uNum<16) return (DREG_HI);
                  	else return ERROR;
                  }
                };

"d"[0-1]?[0-9]".l" { if(yytext[4]==0) 
                  { 
                  	yylval.uNum = yytext[1]-'0';
                  	return(DREG_LO); 
                  }
                  else
                  {
                  	yylval.uNum = 10*(yytext[1]-'0') + yytext[2]-'0';
                  	if(yylval.uNum<16) return (DREG_LO);
                  	else return ERROR;
               
                  	
                  }
                };

"i"[0-1]?[0-9]".h" { if(yytext[4]==0) 
                  { 
                  	yylval.uNum = yytext[1]-'0';
                  	return(IREG_HI); 
                  }
                  else
                  {
                  	yylval.uNum = 10*(yytext[1]-'0') + yytext[2]-'0';
                  	if(yylval.uNum<16) return (IREG_HI);
                  	else return ERROR;
                  }
                };

"i"[0-1]?[0-9]".l" { if(yytext[4]==0) 
                  { 
                  	yylval.uNum = yytext[1]-'0';
                  	return(IREG_LO); 
                  }
                  else
                  {
                  	yylval.uNum = 10*(yytext[1]-'0') + yytext[2]-'0';
                  	if(yylval.uNum<16) return (IREG_LO);
                  	else return ERROR;
               
                  	
                  }
                };

"i"[0-1]?[0-9]  { if(yytext[2]==0) 
                  { 
                  	yylval.uNum = yytext[1]-'0';
                  	return(IREG); 
                  }
                  else
                  {                  
                  	yylval.uNum = 10*(yytext[1]-'0') + yytext[2]-'0';
                  	if(yylval.uNum<16) return (IREG);
                  	else return ERROR;
               
                  	
                  }
                };

"d"[0-1]?[0-9]  { if(yytext[2]==0) 
                  { 
                  	yylval.uNum = yytext[1]-'0';
                  	return(DREG); 
                  }
                  else
                  {
                  	yylval.uNum = 10*(yytext[1]-'0') + yytext[2]-'0';
                  	if(yylval.uNum<16) return (DREG);
                  	else return ERROR;
               
                  	
                  }
                };

[A-Za-z0-9_.]+  yylval.uPChar = yytext; return SYMBOL;



"//!"          return ADDR_LABEL;

"//"[^!][^\n]*     ;


"/*"            {
		int c;
		for ( ; ; ){
			while ( (c = input()) != '*' && c != EOF ) {}

			if ( c == '*' )	{
				while ( (c = input()) == '*' ) {}
				if ( c == '/' ) break;
			}
			if ( c == EOF ) break;
		}
                }





.                return ERROR;



%%
