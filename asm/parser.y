//Copyright (C) 2013 Cem Bassoy.
//
//This file is part of the Sharf CPU.
//
//Sharf CPU is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Sharf CPU is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Sharf CPU.  If not, see <http:--www.gnu.org/licenses/>.


%{ 
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include "asm.h"
	

#define PROLOG_I(var1) \
	int val = (var1)->value;                          \
	free(var1);                                     \
	if(((var1)->defReg&DMEM_DEF)!=0) {                    \
		fprintf(stderr,"%i: ERROR: symbol defined in dmem\n",yylineno); \
		return -1;                             \
	}
                 
#define PROLOG_D(var1) \
	int val = (var1)->value;                          \
	free(var1);                                     \
	if(((var1)->defReg&IMEM_DEF)!=0) {                    \
		fprintf(stderr,"%i: ERROR: symbol defined in imem\n",yylineno); \
		return -1;                             \
	}
	
#define PROLOG_NUM(var1,var2,var3) \
	if((var1)->value>var2 && (var1->defReg&IMEM_DEF)==0 && (var1->defReg&DMEM_DEF)==0 ) {  \
		fprintf(stderr,"%i: Warning: %i bit number out of range\n",yylineno,(var3) ); \
	}                                             \
	var1->value &= var2;




	
	char tmpSymbolString[100];
	char tmpVarString[100];
	
	long long numbers[1000];
	int numIndex = 0;


	enum DefRegion 
	{
		NO_DEF    = 0x0,
		CONST_DEF = 0x1,
		IMEM_DEF  = 0x2,
		DMEM_DEF  = 0x4
	};
	
	struct SymbolDef
	{
		int value;
		enum DefRegion defReg; 
	};


inline int adr2dmemAdr(int adr, int dmemSize) {
	return dmemSize-adr-1;
}
inline int adr2imemAdr(int adr, int imemSize) {
	return imemSize-adr-1;
}

int findConstant(const char *name)
{
	int i;
	for(i=0; i<constTabIndex; i++)
	{
		if(strcmp( constTab[i].name, name)==0 )
			return i;
	}
	return -1;
}
int saveConstant(const char *name, int value)
{
	if(findConstant(name)>=0)                           return -1;
	//if(findLabel(iLabelDefs, label, iLabelDefIndex)>=0) return -1;
	//if(findLabel(dLabelDefs, label, dLabelDefIndex)>=0) return -1;
	
	strcpy( constTab[constTabIndex].name, name );
	constTab[constTabIndex].value = value;
	constTabIndex++;
	return 0;
}



void saveLabelCommand(char *label)
{
	labelCmds[labelCmdIndex].adr = opcodeIndex;
	labelCmds[labelCmdIndex].line = yylineno;
	strcpy(labelCmds[labelCmdIndex].name, label);
	labelCmdIndex++;
}
int saveLabelDef(char *label)
{
	if(findLabel(labelDefs,  label, labelDefIndex) >=0) return -1;
	if(findLabel(iLabelDefs, label, iLabelDefIndex)>=0) return -1;
	if(findLabel(dLabelDefs, label, dLabelDefIndex)>=0) return -1;
	if(findConstant(label) >= 0) return -1;
	
	labelDefs[labelDefIndex].adr = opcodeIndex;
	strcpy(labelDefs[labelDefIndex].name, label);
	
	labelDefIndex++;
	return 0;
}

int saveDLabelDef(char *label, long long *values, int number)
{
	int i;

	if(findLabel(dLabelDefs, label, dLabelDefIndex)>=0) return -1;
	if(findLabel(iLabelDefs, label, iLabelDefIndex)>=0) return -1;
	if(findConstant(label) >= 0) return -1;

	dLabelDefs[dLabelDefIndex].adr = dmemIndex; 
	strcpy(dLabelDefs[dLabelDefIndex].name, label);

	if(values!=0) {
		for(i=0; i<numIndex; i++)
			dmem[dmemIndex++] = values[i];	
	}
	else {
			//leeren speicher reservieren
			dmemIndex+=number;
	}

	numIndex = 0;
	dLabelDefIndex++;

	return 0;
}

int saveILabelDef(char *label, long long *values, int number)
{
	int i;

	if(findLabel(dLabelDefs, label, dLabelDefIndex)>=0) return -1;
	if(findLabel(iLabelDefs, label, iLabelDefIndex)>=0) return -1;
	if(findConstant(label) >= 0) return -1;
	
	iLabelDefs[iLabelDefIndex].adr = imemIndex; 
	strcpy(iLabelDefs[iLabelDefIndex].name, label);

	if(values!=0) {
		for(i=0; i<numIndex; i++)
			imem[imemIndex++] = (int)values[i];
	}
	else {
			//leeren speicher reservieren
			imemIndex+=number;
	}

	numIndex = 0;
	iLabelDefIndex++;

	return 0;
}


void saveAddressLabel(const char *str)
{
	if(properties.addressLabelFile[0]!=0) {
		//nicht ganz so schick, aber egal...
		FILE *file = fopen(properties.addressLabelFile,"a");
		fprintf(file, "%i ?coral1? %s\n",opcodeIndex+properties.codeOffset,str);
		fclose(file);
	}
}



%}

%union {
	char*             uPChar;
	int               uNum;
	struct SymbolDef* uPSymbolDef;
};

%start code
%token SECTION DMEM_SEC IMEM_SEC
%token DWORD IWORD
%token <uNum> DEC_NUM HEX_NUM
%token JMP CALL CJ IJNZ RTS DNOP INOP
%token <uNum> IREG DREG DREG_HI DREG_LO IREG_HI IREG_LO
%token IMEM DMEM DEF_SEC
%token MAIN
%token <uPChar> SYMBOL
%token ERROR
%token EQUAL NEQUAL GEQUAL LEQUAL
%token CLOSE_PAR_H CLOSE_PAR_L MINUS_2 PLUS_2
%token ACC_L ACC_H ACC
%token SHIFT_RIGHT SHIFT_LEFT
%token ADDR_LABEL
%token IO
%token ADD_EQUAL SUB_EQUAL RES_L RES_H RES


%type <uPChar> symbol_str const_str
%type <uNum>   number num18 nums18 num4 num12  num1
%type <uNum>   command indexCmd imemCmd dmemCmd aluCmd controlCmd ioCmd ioReg
%type <uPSymbolDef> num4sym num8sym num9sym num18sym num10sym
%type <uPSymbolDef> arith_expr term factor constant number2

%error-verbose

%%
code       : 
           | initCode MAIN ':' mainCode { int i;
                                          if(saveLabelDef(".main")<0) {
                                          	fprintf(stderr,"ERROR: multiple symbol definition <.main>\n");
                                          	return -1;
                                          }
                                          i = findLabel(labelDefs,".main", labelDefIndex);
                                          labelDefs[i].adr = 0;
                                          
                                        }
           | MAIN ':' mainCode          { int i;
                                          if(saveLabelDef(".main")<0) {
                                          	fprintf(stderr,"ERROR: multiple symbol definition <.main>\n");
                                          	return -1;
                                          }
                                          i = findLabel(labelDefs,".main", labelDefIndex);
                                          labelDefs[i].adr = 0;
                                          
                                        }
           | mainCode
           ;

initCode   : i_section d_section
           | d_section i_section
           | i_section
           | d_section
           | def_section i_section d_section
           | def_section d_section i_section
           | def_section i_section
           | def_section d_section
           ;

i_section  : SECTION IMEM_SEC istatements
           ;
d_section  : SECTION DMEM_SEC dstatements
           ;
           
def_section : SECTION DEF_SEC def_statements
            ;

def_statements : def_statement
               | def_statement def_statements
               ;
               
def_statement : symbol_str '=' num18 ';' {int error = saveConstant($1, $3);
                                          if (error) {fprintf(stderr,"%i: ERROR: multiple symbol definition <%s>\n",yylineno,$1); return -1;}}
              ;
           
istatements  : istatement
             | istatement istatements 
             ;

dstatements  : dstatement
             | dstatement dstatements 
             ;
                          
istatement   
	: symbol_str ':' IWORD nums18 ';'  
		{
			int error = saveILabelDef($1, numbers,0); 
			if (error) {
				fprintf(stderr,"%i: ERROR: multiple label definition <%s>\n",yylineno,(char*)$1); 
				return -1;
			}
		}
	| symbol_str ':' IWORD '[' num18 ']' ';'
		{
			if($5!=0) {
				int error = saveILabelDef($1, 0,$5); 
				if (error) {
					fprintf(stderr,"%i: ERROR: multiple label definition <%s>\n",yylineno,(char*)$1);
					return -1;
				}
			}
		}
	;



dstatement   : symbol_str ':' DWORD nums18 ';' {int error = saveDLabelDef($1, numbers,0); 
                                                if (error) {fprintf(stderr,"%i: ERROR: multiple label definition <%s>\n",yylineno,(char*)$1); return -1;}}
             | symbol_str ':' DWORD '[' num18 ']' ';'{ if($5!=0) {
                                                     	int error = saveDLabelDef($1, 0,$5); 
                                                     	if (error) {fprintf(stderr,"%i: ERROR: multiple label definition <%s>\n",yylineno,(char*)$1); return -1;}
                                                     }
                                                   }
             ; 
             


mainCode     : mainCode statement
             | statement
             ;


statement  : ';'
           | command ';'    {opcode[opcodeIndex++] = $1;  }
           | label ':'
           | ADDR_LABEL SYMBOL    {saveAddressLabel($2);}
           ;
           
label      : SYMBOL         {int error = saveLabelDef($1);
                             if (error) {fprintf(stderr,"%i: ERROR: multiple label definition <%s>\n",yylineno,$1); return -1;}    };

command    : controlCmd {$$=$1;}
           | indexCmd   {$$=$1;}
           | imemCmd    {$$=$1;}
           | dmemCmd    {$$=$1;}
           | aluCmd     {$$=$1;}
           | '#' num18sym   {$$=$2->value; free($2); }
           | ioCmd      {$$=$1;}
           ;


           
controlCmd : JMP  num12  {$$ = 0x00000 + $2;}
           | JMP  SYMBOL {$$ = 0x00000 ; saveLabelCommand($2); }
           | JMP  MAIN   {$$ = 0x00000 | properties.codeOffset;}

           | CJ   num12  {$$ = 0x01000 + $2 ;}
           | CJ   SYMBOL {$$ = 0x01000 ; saveLabelCommand($2); }
           | CJ   MAIN   {$$ = 0x01000 | properties.codeOffset;}

           | CALL num12  {$$ = 0x02000 + $2; if($2==0xfff) warning("\"call 0x1fff\" equals \"rts\"\n"); }
           | CALL SYMBOL {$$ = 0x02000 ; saveLabelCommand($2); }
           | CALL MAIN   {$$ = 0x02000 | properties.codeOffset;}
           
           | IJNZ num12  {$$ = 0x03000 + $2 ;}
           | IJNZ SYMBOL {$$ = 0x03000 ; saveLabelCommand($2); }
           | IJNZ MAIN   {$$ = 0x03000 | properties.codeOffset;}
           
           | JMP  IREG   {$$ = 0x04010 | $2;}
           | RTS         {$$ = 0x0401F}
           | CJ   IREG   {$$ = 0x04020 | $2;}
           | CALL IREG   {$$ = 0x04040 | $2;}
           | IJNZ IREG   {$$ = 0x04080 | $2;}
           
           | DNOP        {$$ = 0x0F000 ;} // i0 = i0
           ;


ioCmd : IREG '=' IO '(' num4 ')'  {$$ = 0x05010 | $5 | ($1<<8);}
      | DREG '=' IO '(' num4 ')'  {$$ = 0x05020 | $5 | ($1<<8);}      
      | ioReg IREG                {$$ = 0x05040 | $2 | ($1<<8);}      
      | ioReg DREG                {$$ = 0x05080 | $2 | ($1<<8);}      
      ;

ioReg : IO  '(' num4 ')' '=' {$$ = $3;}
      ; 

indexCmd   : 'f'  '=' '(' IREG   '<'  IREG ')' {$$ = 0x08100 | ($4<<4) | $6 ; }
           | 'f'  '=' '(' IREG   '>'  IREG ')' {$$ = 0x08100 | ($6<<4) | $4 ; }
           | 'f'  '=' '(' IREG GEQUAL IREG ')' {$$ = 0x08200 | ($4<<4) | $6 ; }
           | 'f'  '=' '(' IREG LEQUAL IREG ')' {$$ = 0x08200 | ($6<<4) | $4 ; }
           | 'f'  '=' '(' IREG EQUAL  IREG ')' {$$ = 0x08400 | ($4<<4) | $6 ; }
           | 'f'  '=' '(' IREG NEQUAL IREG ')' {$$ = 0x08800 | ($4<<4) | $6 ; }

           | IREG '=' IREG '&' IREG            {$$ = 0x09000 | ($1<<8) | ($3<<4) | $5 ; }
           | IREG '=' IREG '|' IREG            {$$ = 0x0A000 | ($1<<8) | ($3<<4) | $5 ; }
           | IREG '=' IREG SHIFT_RIGHT num1    {$$ = 0x0B000 | ($1<<8) | $3;}
           | IREG '=' IREG SHIFT_LEFT  num1    {$$ = 0x0D000 | ($1<<8) | ($3<<4) | $3 ; } // ir = is + is
           | IREG '=' '~' IREG                 {$$ = 0x0B010 | ($1<<8) | $4;}
           
           | IREG '=' IREG '*' IREG            {$$ = 0x0C000 | ($1<<8) | ($3<<4) | $5 ; }
           | IREG '=' IREG '+' IREG            {$$ = 0x0D000 | ($1<<8) | ($3<<4) | $5 ; }
           | IREG ADD_EQUAL IREG               {$$ = 0x0D000 | ($1<<8) | ($1<<4) | $3 ; }  
           | IREG '=' IREG '-' IREG            {$$ = 0x0E000 | ($1<<8) | ($3<<4) | $5 ; }           
           | IREG SUB_EQUAL IREG               {$$ = 0x0E000 | ($1<<8) | ($1<<4) | $3 ; }                      
           | IREG '=' IREG                     {$$ = 0x0F000 | ($1<<8) | $3 ; }

           | IREG_LO '=' num9sym               {PROLOG_I($3) $$ = 0x10000 | (val&0xFF) | ((val&0x100)<<4) | ($1<<8) ; }
           | IREG_HI '=' num9sym               {PROLOG_I($3) $$ = 0x12000 | (val&0xFF) | ((val&0x100)<<4) | ($1<<8) ; }
          
           | IREG ADD_EQUAL num8sym            {PROLOG_I($3) $$ = 0x14000 | (val&0xFF) | ($1<<8) ; } //  (((($3)->value)&0x100)<<4) 
           | IREG SUB_EQUAL num8sym            {PROLOG_I($3) val = ~val; val++; $$ = 0x14000 | (val&0xFF) | ((val&0x100)<<4) | ($1<<8) ; }
           | IREG PLUS_2                       {$$ = 0x14001 | ($1<<8) ;}
           | IREG MINUS_2                      {$$ = 0x150FF | ($1<<8) ;}
           | IREG     '=' num9sym              {PROLOG_I($3) $$ = 0x16000 | (val&0xFF) | ((val&0x100)<<4) | ($1<<8) ; }
           ;


imemCmd    : IREG '=' IMEM '(' num8sym ')'          {PROLOG_I($5) $$ = 0x18000 | ($1<<8) |  val ; }
           | IMEM '(' num8sym ')' '=' IREG          {PROLOG_I($3) $$ = 0x1A000 | (val<<4)|  $6  ; }
           
           | IREG '=' IMEM '(' IREG '+' num4sym ')' {PROLOG_I($7) $$ = 0x1C000 | (val<<4) | ($1<<8) | $5 ; }
           | IREG '=' IMEM '(' IREG             ')' {             $$ = 0x1C000 |            ($1<<8) | $5 ; }
           
           | IMEM '(' IREG '+' num4sym ')' '=' IREG {PROLOG_I($5) $$ = 0x1E000 | (val<<8) | ($8<<4) | $3 ; }
           | IMEM '(' IREG             ')' '=' IREG {             $$ = 0x1E000 |            ($6<<4) | $3 ; }
           ;


dmemCmd    : DREG '=' DMEM '(' num8sym ')'          {PROLOG_D($5) $$ = 0x19000 | ($1<<8)  |  val ; }
           | DMEM '(' num8sym ')' '=' DREG          {PROLOG_D($3) $$ = 0x1B000 | (val<<4) |  $6  ; }
           
           | DREG '=' DMEM '(' IREG '+' num4sym ')' {PROLOG_D($7) $$ = 0x1D000 | (val<<4) | ($1<<8) | $5 ; }
           | DREG '=' DMEM '(' IREG             ')' {             $$ = 0x1D000 |            ($1<<8) | $5 ; }

           | DMEM '(' IREG '+' num4sym ')' '=' DREG {PROLOG_D($5) $$ = 0x1F000 | (val<<8) | ($8<<4) | $3 ; }
           | DMEM '(' IREG             ')' '=' DREG {             $$ = 0x1F000 |            ($6<<4) | $3 ; }
           ;

symbol_str  : SYMBOL { strcpy(tmpSymbolString,$1); $$ = tmpSymbolString;}
            ;


/*
für alu-shift:
if( $6 != 0)  
 {fprintf(stderr,"%i: ERROR: argument != 0\n",yylineno); return -1; }
*/

aluCmd     : 
             DREG '=' DREG SHIFT_RIGHT num1         { $$ = 0x20000 | ($1<<8) | $3; }
           | DREG '=' DREG SHIFT_LEFT num1          { $$ = 0x24000 | ($1<<8) | ($3<<4) | ($3); } //dr=ds+ds
           | DREG '=' '~' DREG                      { $$ = 0x21000 | ($1<<8) | $4; }
           | DREG '='     DREG '&' DREG             { $$ = 0x22000 | ($1<<8) | ($3<<4) | ($5); }
           | DREG '='     DREG '|' DREG             { $$ = 0x23000 | ($1<<8) | ($3<<4) | ($5); }           
           
           | DREG '='     DREG '+' DREG             { $$ = 0x24000 | ($1<<8) | ($3<<4) | ($5); }
           | DREG '='     DREG '-' DREG             { $$ = 0x25000 | ($1<<8) | ($3<<4) | ($5); }
           | DREG '=' DREG                          { $$ = 0x26000 | ($1<<8) | $3; }
           | INOP                                   { $$ = 0x26000  ;                          } //d0=d0	
           | DREG '=' '-' DREG                      { $$ = 0x27000 | ($1<<8) | $4; }
           
           | DREG '=' RES_L                         { $$ = 0x28000 | ($1<<8); }
           | DREG '=' RES                           { $$ = 0x28000 | ($1<<8); }
           | DREG '=' RES_H                         { $$ = 0x29000 | ($1<<8); }
           | RES  '=' DREG '*' DREG                 { $$ = 0x2A000 | ($3<<4) | ($5); }
           //| DREG '='     DREG '*' DREG             { $$ = 0x28000 | ($1<<8) | ($3<<4) | ($5); }
           //| DREG '=' '(' DREG '*' DREG CLOSE_PAR_L { $$ = 0x28000 | ($1<<8) | ($4<<4) | ($6); }
           //| DREG '=' '(' DREG '*' DREG CLOSE_PAR_H { $$ = 0x29000 | ($1<<8) | ($4<<4) | ($6); }           


           //| ACC  '=' num12sym                      { $$ = 0x2C000 | (($3)->value); free($3); }
           //| ACC ADD_EQUAL DREG '*' DREG            { $$ = 0x2D000 | (($3)<<4) | ($5);        }                      
                      
           //| DREG '=' ACC_L                         { $$ = 0x30000 | ($1<<8);               }
           //| DREG '=' ACC_H                         { $$ = 0x31000 | ($1<<8);               }
           | DREG '=' num10sym                       {PROLOG_D($3) $$ = 0x30000 | ($1<<8)| ((val&0x300)<<4) | (val&0xFF) ; }
           
           | 'f' '=' '(' DREG EQUAL  DREG ')'       { $$ = 0x34000 | (($4)<<4) | $6; }
           | 'f' '=' '(' DREG NEQUAL DREG ')'       { $$ = 0x35000 | (($4)<<4) | $6; }
           | 'f' '=' '(' DREG '<'    DREG ')'       { $$ = 0x36000 | (($4)<<4) | $6; }
           | 'f' '=' '(' DREG '>'    DREG ')'       { $$ = 0x36000 | (($6)<<4) | $4; }
           | 'f' '=' '(' DREG GEQUAL DREG ')'       { $$ = 0x37000 | (($4)<<4) | $6; }
           | 'f' '=' '(' DREG LEQUAL DREG ')'       { $$ = 0x37000 | (($6)<<4) | $4; }


           | 'f' '=' '(' DREG EQUAL  num4sym ')'    { $$ = 0x34800 | $4 ; }
           | 'f' '=' '(' DREG NEQUAL num4sym ')'    { $$ = 0x35800 | $4 ; }
           | 'f' '=' '(' DREG '>'    num4sym ')'    { $$ = 0x36800 | $4 ; }
           | 'f' '=' '(' DREG LEQUAL num4sym ')'    { $$ = 0x37800 | $4 ; }
           
           | 'f' '=' num4sym {
                                if ($3->value==0) $$ = 0x35000;  //f=(d0!=d0);
                                else              $$ = 0x34000;  //f=(d0==d0);
                                free($3);
                             }

           //| DREG '=' IREG                          { $$ = 0x38000 | ($1<<8)     | ($3 & 0x7); }
           //| IREG '=' DREG                          { $$ = 0x3A000 | (($1&0x7)<<8) |         $3; }
           ;

nums18     : num18sym            { numbers[numIndex++]= ($1)->value; free($1); }
           | nums18 ',' num18sym { numbers[numIndex++]= ($3)->value; free($3); }
           ;



num18sym     : arith_expr { PROLOG_NUM($1, 0x3ffff, 18) $$ = $1; };
num10sym     : arith_expr { PROLOG_NUM($1, 0x003ff, 10) $$ = $1; }; 
num9sym      : arith_expr { PROLOG_NUM($1, 0x001ff,  9) $$ = $1; }; 
num8sym      : arith_expr { PROLOG_NUM($1, 0x000ff,  8) $$ = $1; }; 
num4sym      : arith_expr { PROLOG_NUM($1, 0x0000f,  4) $$ = $1; };


num1       : number {$$ = ($1 & 0x0001);  if($1>0x0001) {warning("1 bit number out of range\n"); } };
num4       : number {$$ = ($1 & 0x000f);  if($1>0x000f) {warning("4 bit number out of range\n"); } };
num12      : number {$$ = ($1 & 0x0fff);  if($1>0x0fff) {warning("12 bit number out of range\n"); } };
num18      : number {$$ = ($1 & 0xffff);  if($1>0xffff) {warning("18 bit number out of range\n"); } };
           

arith_expr : term                 { $$ = $1;     }
           | '+' term             { $$ = $2;     }
           | '-' term             { ($2)->value = -($2)->value; $$ = $2;     }
           | arith_expr '+' term  { ($1)->defReg |= ($3)->defReg;
                                    ($1)->value  += ($3)->value;
                                    free($3);
                                    $$ = $1;
                                  }
           | arith_expr '-' term  { ($1)->defReg |= ($3)->defReg;
                                    ($1)->value  -= ($3)->value;
                                    free($3);
                                    $$ = $1;
                                  }
           | '(' arith_expr CLOSE_PAR_L  { $2->value = ( $2->value    )&0x1ff; $$=$2; }
           | '(' arith_expr CLOSE_PAR_H  { $2->value = (($2->value)>>9)&0x1ff; $$=$2; }
           ;
           
term       : factor           {$$ = $1;     }
           | factor '*' term  { ($1)->defReg |= ($3)->defReg;
                                ($1)->value  *= ($3)->value;
                                free($3);
                                $$ = $1;
                              }
           | factor '/' term  { ($1)->defReg |= ($3)->defReg;
                                ($1)->value  /= ($3)->value;
                                free($3);
                                $$ = $1;
                              }
			| factor SHIFT_LEFT term { ($1)->defReg |= ($3)->defReg;
                                ($1)->value  <<= ($3)->value; //printf("<< %i %i\n", ($1)->value ,($3)->value);
                                free($3);
                                $$ = $1;
                              }
			| factor SHIFT_RIGHT term { ($1)->defReg |= ($3)->defReg;  //printf(">> %x\n", ($1)->value );
                                ($1)->value  >>= ($3)->value;  //printf(">> %x %x\n", ($1)->value ,($3)->value);
                                free($3);
                                $$ = $1;
                              }

			| factor '&' term { ($1)->defReg |= ($3)->defReg;
                                ($1)->value  &= ($3)->value;
                                free($3);
                                $$ = $1;
                              }
           ;



factor     : constant          {$$ = $1;}
           | number2           {$$ = $1;}
           | '(' arith_expr ')'{$$ = $2;}
           ;



constant   : const_str   {int index;
                          struct SymbolDef *er = (struct SymbolDef*)malloc(sizeof(struct SymbolDef));

                          if      ( (index=findConstant($1)) > -1) {
                          	er->value = constTab[index].value; er->defReg = CONST_DEF;
                          }
                          else if ( (index=findLabel(iLabelDefs, $1, iLabelDefIndex)) > -1) {
                          	er->value = adr2imemAdr( iLabelDefs[index].adr, imemSize ); er->defReg = IMEM_DEF;
                          }
                          else if ( (index=findLabel(dLabelDefs, $1, dLabelDefIndex)) > -1) {
                          	er->value = adr2dmemAdr( dLabelDefs[index].adr, dmemSize ); er->defReg = DMEM_DEF;
                          }
                          else {
                          	fprintf(stderr,"%i: ERROR: label <%s> not defined\n",yylineno,$1); return -1;
                          }
                          $$ = er;
                         }
           ;

const_str  : SYMBOL { strcpy(tmpVarString, $1); $$ = tmpVarString;}
           ;

number2    : number      {
                          struct SymbolDef *er = (struct SymbolDef*)malloc(sizeof(struct SymbolDef));
                          er->defReg = NO_DEF;
                          er->value  = $1;
                          $$ = er;
                         }
           ;

number     : DEC_NUM {$$=$1;}
           | HEX_NUM {$$=$1;}
           ;
           


%%
