//Copyright (C) 2013 Cem Bassoy.
//
//This file is part of the Sharf CPU.
//
//Sharf CPU is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Sharf CPU is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Sharf CPU.  If not, see <http:--www.gnu.org/licenses/>.


#ifndef ASM_H
#define ASM_H

	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	

                 
                 
	extern int yylineno;
	extern int yydebug;
	int yylex();
	int yyparse();
	int yyerror(char *s);

	struct Properties
	{
		char addressLabelFile[256];
		int codeOffset;
	};
	
	extern struct Properties properties;

	struct ConstTab
	{
		char name[100];
		int value;
	} ;
	
	struct LabelTab
	{
		int adr;
		char name[100];
		int line;
	} ;

	
	extern struct LabelTab labelDefs[];
	extern int labelDefIndex;

	extern struct LabelTab labelCmds[];
	extern int labelCmdIndex;

	extern struct LabelTab dLabelDefs[];
	extern int dLabelDefIndex;

	extern struct LabelTab iLabelDefs[];
	extern int iLabelDefIndex;

	extern struct ConstTab constTab[];
	extern int constTabIndex;
	
	extern int imemSize;
	extern int dmemSize;



	extern int opcode[];
	extern int opcodeIndex;
	
	extern long long dmem[];
	extern int dmemIndex;

	extern int imem[];
	extern int imemIndex;
	
	int findLabel(const struct LabelTab *labelDefs, const char *label, int count);
	void warning(const char *str);
	void int2bin(long long val, char* c, int word_len);



#endif

