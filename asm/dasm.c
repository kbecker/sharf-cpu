//Copyright (C) 2013 Cem Bassoy.
//
//This file is part of the Sharf CPU.
//
//Sharf CPU is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Sharf CPU is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Sharf CPU.  If not, see <http:--www.gnu.org/licenses/>.


#include <stdio.h>   //file and screen i/o
#include <string.h>  //string handling

#include "dasm.h"
#include "util.h"

inline int getIR(int opcode) { return (opcode&0xF00)>>8; }
inline int getIT(int opcode) { return (opcode&0x0F0)>>4; }
inline int getIS(int opcode) { return (opcode&0x00F); }
inline int getDR(int opcode) { return (opcode&0xF00)>>8; }
inline int getDT(int opcode) { return (opcode&0x0F0)>>4; }
inline int getDS(int opcode) { return (opcode&0x00F); }

inline int getABS(int opcode) { return (opcode&0xFFF); }
inline int getNPos(int opcode)  { return opcode&0xFF; }
inline int getNNeg(int opcode)  { return (~(((opcode&0x1000)>>4) | (opcode&0xFF)))+1; }
inline int getN(int opcode)  { return ((opcode&0x1000)>>4) | (opcode&0xFF); }

void jmp_a                    (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"jmp 0x%x"           ,getABS(mcode)); }
void call_a                   (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"call 0x%x"          ,getABS(mcode)); }
void cjf_a                    (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"cjf 0x%x"           ,getABS(mcode)); }
void ijnz_a                   (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"ijnz 0x%x"          ,getABS(mcode)); }

void rts_is                   (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"rts"                             ); }
void jmp_is                   (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"jmp i%i"            ,getIS(mcode)); }
void cjf_is                   (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"cjf i%i"            ,getIS(mcode)); }
void call_is                  (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"call i%i"           ,getIS(mcode)); }
void ijnz_is                  (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"ijnz i%i"           ,getIS(mcode)); }

void ir_eq_io_a               (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"i%i=io(%i)"         ,getIR(mcode), getIS(mcode)); }
void io_a_eq_is               (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"io(%i)=i%i"         ,getIR(mcode), getIS(mcode)); }
void dr_eq_io_a               (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"d%i=io(%i)"         ,getDR(mcode), getDS(mcode)); }
void io_a_eq_ds               (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"io(%i)=d%i"         ,getDR(mcode), getDS(mcode)); }

void f_eq_it_leq_is           (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"f=i%i<i%i"          ,getIT(mcode), getIS(mcode)); }
void f_eq_it_ge_is            (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"f=i%i>=i%i"         ,getIT(mcode), getIS(mcode)); }
void f_eq_it_eq_is            (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"f=i%i==i%i"         ,getIT(mcode), getIS(mcode)); }
void f_eq_it_neq_is           (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"f=i%i!=i%i"         ,getIT(mcode), getIS(mcode)); }

void ir_eq_it_and_is          (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"i%i=i%i&i%i"        ,getIR(mcode), getIT(mcode), getIS(mcode)); }
void ir_eq_it_or_is           (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"i%i=i%i|i%i"        ,getIR(mcode), getIT(mcode), getIS(mcode)); }
void ir_eq_is_shr             (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"i%i=i%i>>1"         ,getIR(mcode), getIS(mcode)); }
void ir_eq_not_is             (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"i%i=~i%i"           ,getIR(mcode), getIS(mcode)); }

void ir_eq_it_mul_is          (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"i%i=i%i*i%i"        ,getIR(mcode), getIT(mcode), getIS(mcode)); }
void ir_eq_it_add_is          (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"i%i=i%i+i%i"        ,getIR(mcode), getIT(mcode), getIS(mcode)); }
void ir_eq_it_sub_is          (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"i%i=i%i-i%i"        ,getIR(mcode), getIT(mcode), getIS(mcode)); }
void ir_eq_is                 (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"i%i=i%i"            ,getIR(mcode), getIS(mcode)); }
void dnop                     (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"dnop"               );}

void ir_l_eq_n                (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"i%i.l=0x%x"         ,getIR(mcode), getN(mcode));}
void ir_h_eq_n                (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"i%i.h=0x%x"         ,getIR(mcode), getN(mcode));}

void ir_add_eq_n              (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"i%i+=%i"            ,getIR(mcode), getNPos(mcode));}
void ir_sub_eq_n              (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"i%i-=%i"            ,getIR(mcode), getNNeg(mcode));}
void ir_add_add               (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"i%i++"              ,getIR(mcode));}
void ir_sub_sub               (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"i%i--"              ,getIR(mcode));}

void ir_eq_n                  (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"i%i=%i"             ,getIR(mcode), getN(mcode));}
void ir_eq_imem_a             (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"i%i=imem(%i)"       , getIR(mcode), mcode&0xFF); }
void dr_eq_dmem_a             (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"d%i=dmem(%i)"       , getDR(mcode), mcode&0xFF); }

void imem_a_eq_is             (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"imem(%i)=i%i"       , (mcode&0xFF)>>4 , getIS(mcode)); }
void dmem_a_eq_ds             (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"dmem(%i)=d%i"       , (mcode&0xFF)>>4 , getDS(mcode)); }

void ir_eq_imem_is            (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"i%i=imem(i%i)"      , getIR(mcode), getIS(mcode)); }
void ir_eq_imem_is_sub_o      (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"i%i=imem(i%i+%i)"   , getIR(mcode), getIS(mcode), (mcode&0xF0)>>4 ); }
void dr_eq_dmem_is            (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"d%i=dmem(i%i)"      , getDR(mcode), getIS(mcode)); }
void dr_eq_dmem_is_sub_o      (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"d%i=dmem(i%i+%i)"   , getDR(mcode), getIS(mcode), (mcode&0xF0)>>4 ); }

void imem_is_eq_it            (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"imem(i%i)=i%i"      , getIS(mcode), getIT(mcode)); }
void imem_is_sub_o_eq_it      (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"imem(i%i+%i)=i%i"   , getIS(mcode), (mcode&0xF00)>>8, getIT(mcode)); }
void dmem_is_eq_dt            (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"dmem(i%i)=d%i"      , getIS(mcode), getDT(mcode)); }
void dmem_is_sub_o_eq_dt      (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"dmem(i%i+%i)=d%i"   , getIS(mcode), (mcode&0xF00)>>8, getDT(mcode)); }

//void acc_n                    (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"acc=0x%x"           ,mcode&0xfff); }
//void dr_eq_acc_l              (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"d%i=acc.l"          ,getDR(mcode)); }
//void dr_eq_acc_h              (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"d%i=acc.h"          ,getDR(mcode)); }
//void acc_plus_eq_dt_mul_ds    (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"acc+=d%i*d%i"       ,getDT(mcode), getDS(mcode));}

void f_eq_dt_eq_0             (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"f=(d%i==0)"         ,getDT(mcode)); }
void f_eq_dt_neq_0            (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"f=(d%i!=0)"         ,getDT(mcode)); }
void f_eq_dt_gr_0             (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"f=(d%i>0)"          ,getDT(mcode)); }
void f_eq_dt_leq_0            (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"f=(d%i<=0)"         ,getDT(mcode)); }

void f_eq_1                   (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"f=1"                ); }
void f_eq_dt_eq_ds            (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"f=(d%i==d%i)"       ,getDT(mcode),getDS(mcode)); }
void f_eq_0                   (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"f=0"                ); }
void f_eq_dt_neq_ds           (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"f=(d%i!=d%i)"       ,getDT(mcode),getDS(mcode)); }
void f_eq_dt_geq_ds           (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"f=(d%i>=d%i)"       ,getDT(mcode),getDS(mcode)); }
void f_eq_dt_le_ds            (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"f=(d%i<d%i)"        ,getDT(mcode),getDS(mcode)); }
void f_eq_dt_gr_ds            (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"f=(d%i>d%i)"        ,getDT(mcode),getDS(mcode)); }
void f_eq_dt_leq_ds           (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"f=(d%i<=d%i)"       ,getDT(mcode),getDS(mcode)); }

void dr_eq_n                  (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"d%i=0x%x"           , getDR(mcode), (mcode&0xff) | ((mcode&0x3000)>>4) ); }
void dr_eq_dt_add_ds          (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"d%i=d%i+d%i"        , getDR(mcode), getDT(mcode), getDS(mcode)); }
void dr_eq_dt_sub_ds          (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"d%i=d%i-d%i"        , getDR(mcode), getDT(mcode), getDS(mcode)); }

void dr_eq_dt_and_ds          (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"d%i=d%i&d%i"        , getDR(mcode), getDT(mcode), getDS(mcode)); }
void dr_eq_dt_or_ds           (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"d%i=d%i|d%i"        , getDR(mcode), getDT(mcode), getDS(mcode)); }
void dr_eq_ds_sr              (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"d%i=d%i>>1"         , getDR(mcode), getDS(mcode)); }
void dr_eq_inv_ds             (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"d%i=~d%i"           , getDR(mcode), getDS(mcode)); }

void inop                     (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"inop"               );}
void dr_eq_ds                 (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"d%i=d%i"            , getDR(mcode), getDS(mcode)); }
void dr_eq_neg_ds             (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"d%i=-d%i"           , getDR(mcode), getDS(mcode)); }

void dr_eq_mult_res_l        (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"d%i=mres.l"          , getDR(mcode)); }
void dr_eq_mult_res_h        (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"d%i=mres.l"          , getDR(mcode)); }
void dt_mul_ds               (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"(d%i*d%i)"           , getDT(mcode), getDS(mcode)); }

//void dr_eq_dt_mul_ds_l        (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"d%i=(d%i*d%i).l"    , getDR(mcode), getDT(mcode), getDS(mcode)); }
//void dr_eq_dt_mul_ds_h        (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"d%i=(d%i*d%i).h"    , getDR(mcode), getDT(mcode), getDS(mcode)); }

//void dr_eq_is                 (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"d%i=i%i"            , getDR(mcode), getIS(mcode)); }
//void ir_eq_ds                 (char *asmStr, const unsigned int mcode) {sprintf(asmStr,"i%i=d%i"            , getIR(mcode), getDS(mcode)); }

const struct CPUCmd CPUCmds[] =
{

//{OP_CODE, MASK_OP, function                , color },
	{0x00000, 0x3F000, jmp_a                   , 0 },  // jmp a             : 00 00 00 aaaa aaaa aaaa : 0x00aaa	
	{0x01000, 0x3F000, cjf_a                   , 0 },  // cjf a             : 00 00 01 aaaa aaaa aaaa : 0x01aaa
	{0x02000, 0x3F000, call_a                  , 0 },  // call a            : 00 00 10 aaaa aaaa aaaa : 0x02aaa	
	{0x03000, 0x3F000, ijnz_a                  , 0 },  // ijnz a            : 00 00 11 aaaa aaaa aaaa : 0x03aaa

	{0x0401F, 0x3E0FF, rts_is                  , 0 },  // ijnz is           : 00 01 0* **** 0001 1111 : 0x04*8F	
	{0x04010, 0x3E0F0, jmp_is                  , 0 },  // jmp is            : 00 01 0* **** 0001 ssss : 0x04*1s // wenn tttt=0xF -> rts
	{0x04020, 0x3E0F0, cjf_is                  , 0 },  // cjf is            : 00 01 0* **** 0010 ssss : 0x04*2s
	{0x04040, 0x3E0F0, call_is                 , 0 },  // call is           : 00 01 0* **** 0100 ssss : 0x04*4s
	{0x04080, 0x3E0F0, ijnz_is                 , 0 },  // ijnz is           : 00 01 0* **** 1000 ssss : 0x04*8s


	{0x06010, 0x3E0F0, ir_eq_io_a              , 0 },  // ir = io(a)        : 00 01 1* rrrr 0001 aaaa : 0x06r1a
	{0x06020, 0x3E0F0, dr_eq_io_a              , 0 },  // dr = io(a)        : 00 01 1* rrrr 0010 aaaa : 0x06r2a
	{0x06040, 0x3E0F0, io_a_eq_is              , 0 },  // io(a) = is        : 00 01 1* aaaa 0100 ssss : 0x06a4s
	{0x06080, 0x3E0F0, io_a_eq_ds              , 0 },  // io(a) = ds        : 00 01 1* aaaa 1000 ssss : 0x06a8s

	{0x08100, 0x3FF00, f_eq_it_ge_is           , 0 },  // f  = it > is      : 00 10 00 0001 tttt ssss : 0x081ts
	{0x08200, 0x3FF00, f_eq_it_leq_is          , 0 },  // f  = it <= is     : 00 10 00 0010 tttt ssss : 0x082ts
	{0x08400, 0x3FF00, f_eq_it_eq_is           , 0 },  // f  = it == is     : 00 10 00 0100 tttt ssss : 0x084ts
	{0x08800, 0x3FF00, f_eq_it_neq_is          , 0 },  // f  = it != is     : 00 10 00 1000 tttt ssss : 0x088ts
	
	{0x09000, 0x3F000, ir_eq_it_and_is         , 1 },  // ir = it&is        : 00 10 01 rrrr tttt ssss : 0x09rts
	{0x0A000, 0x3F000, ir_eq_it_or_is          , 1 },  // ir = it|is        : 00 10 10 rrrr tttt ssss : 0x0Arts
	{0x0B000, 0x3F010, ir_eq_is_shr            , 1 },  // ir = is>>1        : 00 10 11 rrrr ***0 ssss : 0x0Br0s
	{0x0B010, 0x3F010, ir_eq_not_is            , 1 },  // ir = ~is          : 00 10 11 rrrr ***1 ssss : 0x0Br1s
	
	{0x0C000, 0x3F000, ir_eq_it_mul_is         , 1 },  // ir = it*is        : 00 11 00 rrrr tttt ssss : 0x0Crts
	{0x0D000, 0x3F000, ir_eq_it_add_is         , 1 },  // ir = it+is        : 00 11 01 rrrr tttt ssss : 0x0Drts
	{0x0E000, 0x3F000, ir_eq_it_sub_is         , 1 },  // ir = it-is        : 00 11 10 rrrr tttt ssss : 0x0Erts
	{0x0F000, 0x3F000, ir_eq_is                , 1 },  // ir = is           : 00 11 11 rrrr **** ssss : 0x0Fr*s
	{0x0F000, 0x3FF0F, dnop                    , 1 },  // dnop              : 00 11 11 0000 **** 0000 : 0x0F0*0

	{0x10000, 0x3E000, ir_l_eq_n               , 1 },  // ir.l = n          : 01 00 0n rrrr nnnn nnnn : 0x10rnn
	{0x12000, 0x3E000, ir_h_eq_n               , 1 },  // ir.h = n          : 01 00 1n rrrr nnnn nnnn : 0x12rnn

	{0x14001, 0x3F0FF, ir_add_add              , 1 },  // ir += n           : 01 01 00 rrrr 0000 0001 : 0x14rnn
	{0x150FF, 0x3F0FF, ir_sub_sub              , 1 },  // ir -= n           : 01 01 01 rrrr 1111 1111 : 0x14rnn  

	{0x14000, 0x3F000, ir_add_eq_n             , 1 },  // ir += n           : 01 01 00 rrrr nnnn nnnn : 0x14rnn
	{0x15000, 0x3F000, ir_sub_eq_n             , 1 },  // ir -= n           : 01 01 01 rrrr nnnn nnnn : 0x14rnn  
	{0x16000, 0x3E000, ir_eq_n                 , 1 },  // ir  = n           : 01 01 1n rrrr nnnn nnnn : 0x16rnn

	{0x18000, 0x3F000, ir_eq_imem_a            , 2},   // ir=imem(a)        : 01 10 00 rrrr aaaa aaaa : 0x18raa
	{0x19000, 0x3F000, dr_eq_dmem_a            , 2 },  // dr=dmem(a)        : 01 10 01 rrrr aaaa aaaa : 0x19raa
	{0x1A000, 0x3F000, imem_a_eq_is            , 2 },  // imem(a)=is        : 01 10 10 aaaa aaaa ssss : 0x1Aaas
	{0x1B000, 0x3F000, dmem_a_eq_ds            , 2 },  // dmem(a)=ds        : 01 10 11 aaaa aaaa ssss : 0x1Baas
	
	{0x1C000, 0x3F0F0, ir_eq_imem_is           , 2 },  // ir=imem(is)       : 01 11 00 rrrr 0000 ssss : 0x1Cros
	{0x1C000, 0x3F000, ir_eq_imem_is_sub_o     , 2 },  // ir=imem(is+o)     : 01 11 00 rrrr oooo ssss : 0x1Cros
	{0x1D000, 0x3F0F0, dr_eq_dmem_is           , 2 },  // dr=dmem(is)       : 01 11 01 rrrr 0000 ssss : 0x1Dros
	{0x1D000, 0x3F000, dr_eq_dmem_is_sub_o     , 2 },  // dr=dmem(is+o)     : 01 11 01 rrrr oooo ssss : 0x1Dros
	
	{0x1E000, 0x3FF00, imem_is_eq_it           , 2 },  // imem(is)=it       : 01 11 10 0000 tttt ssss : 0x1Eots
	{0x1E000, 0x3F000, imem_is_sub_o_eq_it     , 2 },  // imem(is+o)=it     : 01 11 10 oooo tttt ssss : 0x1Eots
	{0x1F000, 0x3FF00, dmem_is_eq_dt           , 2 },  // dmem(is)=dt       : 01 11 11 0000 tttt ssss : 0x1Fots
	{0x1F000, 0x3F000, dmem_is_sub_o_eq_dt     , 2 },  // dmem(is+o)=dt     : 01 11 11 oooo tttt ssss : 0x1Fots

	{0x20000, 0x3f000, dr_eq_ds_sr             , 4 },  // dr= ds>>1
	{0x21000, 0x3f000, dr_eq_inv_ds            , 4 },  // dr=~ds
	{0x26000, 0x3ff0f, inop                    , 4 },  // d0=d0
	{0x26000, 0x3f000, dr_eq_ds                , 4 },  // dr= ds 
	{0x27000, 0x3f000, dr_eq_neg_ds            , 4 },  // dr=-ds
	{0x30000, 0x3C000, dr_eq_n                 , 4 },  // dr=n
	
	{0x24000, 0x3f000, dr_eq_dt_add_ds         , 4 }, // dr=dt+ds
	{0x25000, 0x3f000, dr_eq_dt_sub_ds         , 4 }, // dr=dt-ds
	{0x22000, 0x3f000, dr_eq_dt_and_ds         , 4 }, // dr=dt&ds
	{0x23000, 0x3f000, dr_eq_dt_or_ds          , 4 }, // dr=dt|ds
	

	{0x28000, 0x3f000, dr_eq_mult_res_l       , 4 }, // dr=mres.l
	{0x29000, 0x3f000, dr_eq_mult_res_h       , 4 }, // dr=mres.h
	{0x2A000, 0x3f000, dt_mul_ds              , 4 }, // dt*ds
	
	//{0x28000, 0x3f000, dr_eq_dt_mul_ds_l       , 4 }, // dr=(dt*ds).l
	//{0x29000, 0x3f000, dr_eq_dt_mul_ds_h       , 4 }, // dr=(dt*ds).h
		
	//{0x2c000, 0x3f000, acc_n                   , 4 }, // acc=n
	//{0x30000, 0x3f000, dr_eq_acc_l             , 4 }, // dr=acc.l	
	//{0x31000, 0x3f000, dr_eq_acc_h             , 4 }, // dr=acc.h
	//{0x2D000, 0x3f000, acc_plus_eq_dt_mul_ds   , 4 }, // acc+=dt*ds
	
	{0x34000, 0x3F8ff, f_eq_1                  , 5 }, // f = 1
	{0x34800, 0x3F800, f_eq_dt_eq_0            , 5 }, // f = (dt == 0)
	{0x34000, 0x3F800, f_eq_dt_eq_ds           , 5 }, // f = (dt == ds)
	{0x35000, 0x3F8ff, f_eq_0                  , 5 }, // f = 0
	{0x35800, 0x3F800, f_eq_dt_neq_0           , 5 }, // f = (dt != 0)
	{0x35000, 0x3F800, f_eq_dt_neq_ds          , 5 }, // f = (dt != ds)
	{0x36800, 0x3F800, f_eq_dt_gr_0            , 5 }, // f = (dt > 0)
	{0x36000, 0x3F800, f_eq_dt_le_ds           , 5 }, // f = (dt < ds)
	{0x37800, 0x3F800, f_eq_dt_leq_0           , 5 }, // f = (dt <= 0)
	{0x37000, 0x3F800, f_eq_dt_geq_ds          , 5 }, // f = (dt >= ds)
	
	{0x00000, 0x00000, NULL                    , 6 }, // end of list
};




int bin2int(const char *binStr)
{
	int num = 0;
	while(*binStr && *binStr!=10)
	{
		num = 2*num+(*binStr++)-'0';
	}
	return num;
}

void setPrintStr(char *printStr, const char *fileStr)
{
	printStr[0] = fileStr[0];
	printStr[1] = fileStr[1];
	printStr[2] = fileStr[2];
	printStr[3] = fileStr[3];
	
	printStr[4] = ' ';
	
	printStr[5] = fileStr[4];
	printStr[6] = fileStr[5];
	
	printStr[7] = ' ';	
	
	printStr[8] = fileStr[6];
	printStr[9] = fileStr[7];
	printStr[10] = fileStr[8];
	printStr[11] = fileStr[9];
	
	printStr[12] = ' ';	
	
	printStr[13] = fileStr[10];
	printStr[14] = fileStr[11];
	printStr[15] = fileStr[12];
	printStr[16] = fileStr[13];
	
	printStr[17] = ' ';	
	
	printStr[18] = fileStr[14];
	printStr[19] = fileStr[15];
	printStr[20] = fileStr[16];
	printStr[21] = fileStr[17];
	
	printStr[22] = 0;
	
}

int disassemble(char *fileName, unsigned int codeOffset)
{
	FILE *srcFile  = NULL;
	char asmStr[256], fileStr[20], printStr[25];

	int error = 0, opcode=1, oldOpcode;
	int instrIndex = 0;
	int ascii;

	srcFile  = fopen(fileName,  "r");
	
	
	if (!srcFile)
	{
		printf("error opening <%s>\n",fileName);
		return -1;
	}
	
	//test if input file is binary
	fgets(fileStr, 20, srcFile);
	
	if(fileStr[18]==10 && fileStr[19]==0) {
		ascii=1;
		//move to first valid command
		fseek ( srcFile , codeOffset*19 , SEEK_SET );
	}
	else {
		ascii=0;
		//move to first valid command
		fseek ( srcFile , codeOffset*4 , SEEK_SET );
	}

	
	
	while( (!error) && (!feof(srcFile)) )
	{
		oldOpcode = opcode;
		
		if(ascii) {
			fgets(fileStr, 20, srcFile);
			opcode = bin2int(fileStr);
		}
		else {
			fread(&opcode,4,1,srcFile);
			int2bin(opcode, fileStr,18);
		}
		
		//disassembler bricht ab, wenn zwei mal hintereinander jp 0x0 gelesen wurde
		if(opcode==0 && oldOpcode==0) break;

		TDAssembleFcn dAssembleFcn = getDAssembleFcn(opcode);

		setPrintStr(printStr, fileStr);

		if (dAssembleFcn!=NULL)
		{
			(*dAssembleFcn)(asmStr, opcode);
			printf("[0x%04x] %s  %s\n", instrIndex2Adr(instrIndex, codeOffset),printStr,asmStr);
		}
		else
			printf("[0x%04x] %s  invalid\n", instrIndex2Adr(instrIndex, codeOffset),printStr);
		instrIndex++;
		
	}
	fclose(srcFile);

	return 0;
}


TDAssembleFcn getDAssembleFcn(const unsigned int mcode)
{
	int i=0;
	
	while(1)
	{
		if( (CPUCmds[i].cmdAssembleMaskValid & mcode)  == CPUCmds[i].cmdAssembleMask )
		{
			return CPUCmds[i].cmdDAssembleFcn;
		}
		i++;
	}
	
	return NULL;
}
