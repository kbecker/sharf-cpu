//Copyright (C) 2013 Cem Bassoy.
//
//This file is part of the Sharf CPU.
//
//Sharf CPU is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Sharf CPU is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Sharf CPU.  If not, see <http:--www.gnu.org/licenses/>.


#include "util.h"


void int2bin(long long val, char* c, int word_len) 
{
	int i;
	for(i=word_len-1; i>=0; i--){
		*c++ = ((val>>i)&1) + '0';
	}
	*c = 0;	    
}


inline int instrIndex2Adr(int index, int codeOffset) {
	return index+codeOffset;
}
