CPU1C Assembler Version 0.4.5 beta
==================================

Changelog zu 0.4.3 beta:
	im DMEM fangen Daten bei höchster Adresse an und wachsen nach unten
	dementsprechend ist die Notation z.B. ir=imem(is-offset) oder dr=imem(is-offset)
	Definition von Variablen in eigener Sektion möglich
	statt Nummern können überall beliebige arithmetische Ausdrücke mit Zahlen, Variablen und Labels eingegeben werden
		(außer bei Befehlen für den Kontrollfluss wie jmp, call, ... Hier sind nur Labels und Zahlen erlaubt.)


Changelog zu 0.4.1 beta:
	anderer Befehlssatz, IMEM und DMEM wird jetzt mit absoluter 9-bit Adresse adressiert
	im IMEM fangen Befehle bei 0 an und wachsen nach oben, Daten fangen bei höchster Adresse an und wachsen nach unten
	im DMEM fangen Daten bei 0 an und wachsen nach oben


Aufruf:
	cpu1c-asm source dest
	Übersetzt source nach dest, erzeugt gleichzeitig 'dram.dat'
	
Quellcode:
	parser.y
	scanner.l

Beispielprogramm:
	prog.txt

Mögliche Befehle:
	...

Bekannte Bugs:
	'//'-Kommentar im '/* */'-Kommentar ergibt Syntaxfehler


