#! /bin/sh

# ./sim.sh examples/macc.asm waves/macc.sav 10us

# -E : Stop after the preprocessing stage
# -x language:  specify the language 
# -C : do not discard comments
#gcc -E -x c -C $1 | ./sharf - $* -g


rm -f vhdl/iram.dat vhdl/dram.dat cpu_tb cpu_tb.ghw work/*
rm -f ${1}~

echo "gcc -E -x c -P -C $1 > ${1}~; ./asm/cpu1c-asm ${1}~ -o vhdl/iram.dat vhdl/dram.dat -a -e 0x0 -w 32" # -w 32 

gcc -E -x c -P -C $1 > ${1}~; ./asm/cpu1c-asm ${1}~ -o vhdl/iram.dat vhdl/dram.dat -a -e 0x0 -w 32 #-l filter.txt # -e 0x22


#gcc -E -x c -P -C examples/fak2.asm > examples/fak2.asm~; ./asm/cpu1c-asm examples/fak2.asm~ -o vhdl/iram.dat vhdl/dram.dat -a -e 0x0
#./asm/cpu1c-asm asm/examples/io1.asm -o iram1.dat dram1.dat -a -e 0x0 #-l filter.txt
#./asm/cpu1c-asm asm/examples/io2.asm -o iram2.dat dram2.dat -a -e 0x0 #-l filter.txt

ghdl -i --workdir=work vhdl/*.vhd

ghdl -m -Punisim --workdir=work --ieee=synopsys -fexplicit cpu_tb

ghdl -r -Punisim --ieee=synopsys -fexplicit cpu_tb --wave=cpu_tb.ghw --stop-time=$3 2>&1  | grep -v "warning"

gtkwave cpu_tb.ghw $2 &
