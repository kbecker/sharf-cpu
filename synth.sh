#! /bin/sh

design=CPU



cd ise

if [ "$1" = "-p" ]; then
	bitgen ${design}2.ncd ${design}.bit > log/bitgen.log;    echo "Bitgeneration process finished."
	export LD_PRELOAD=~/usb-driver/libusb-driver.so
	echo "setMode -bs" > com.cmd; 
	echo "setCable -port auto" >> com.cmd; 
	echo "Identify" >> com.cmd;
	echo "assignFile -p 3 -file \"${design}.bit\" " >> com.cmd;	
	echo "Program -p 3" >> com.cmd; 
	echo "quit" >> com.cmd;
	impact -batch com.cmd
#--------------------------------------------------------------------------------------------------
elif [ "$1" = "-c" ]; then
	 xst -intstyle ise -ifn $design.xst -ofn $design.stx
#--------------------------------------------------------------------------------------------------
elif [ "$1" = "-s" ]; then
	if [ ! -e "log" ]; then mkdir log; fi
	if [ -f "log/xst.log" ]; then rm log/xst.log; fi
	xst -ifn $design.xst;        echo "Synthesis process finished."
#--------------------------------------------------------------------------------------------------
elif [ "$1" = "-r" ]; then
	#../clean.sh;	
	xst -ifn $design.xst                   echo "Synthesis process finished."
	if [ -f "$design.ucf" ]; then 
		ngdbuild $design.ngc -uc $design.ucf echo "Translation process finished."
	else
		ngdbuild $design.ngc                 echo "Translation process finished."
	fi	
	map $design.ngd                        echo "Mapping process finished." 
	par $design.ncd ${design}2.ncd         echo "Place and Route process finished."
#--------------------------------------------------------------------------------------------------
elif [ "$1" = "-sp" ]; then
	../clean.sh	
	xst -ifn $design.xst > log/xst.log;                       echo "Synthesis process finished."
	if [ -f "$design.ucf" ]; then
		ngdbuild $design.ngc -uc $design.ucf > log/ngd.log;     echo "Translation process finished."
	else
		ngdbuild $design.ngc > log/ngd.log;                     echo "Translation process finished."
	fi	
	map $design.ngd > log/map.log;                            echo "Mapping process finished." 
	par $design.ncd ${design}2.ncd > log/par.log;             echo "Place and Route process finished."
	bitgen ${design}.ncd > log/bitgen.log;                    echo "Bitgeneration process finished."
	export LD_PRELOAD=/home/ti6csb/usb-driver/libusb-driver.so
	echo "setMode -bs" > com.cmd
	echo "setCable -port auto" >> com.cmd
	echo "Identify" >> com.cmd
	echo "assignFile -p 3 -file \"${design}.bit\" " >> com.cmd
	echo "Program -p 3" >> com.cmd
	echo "quit" >> com.cmd	
	impact -batch com.cmd
#--------------------------------------------------------------------------------------------------
else
	echo "-p  : only program the fpga"
	echo "-s  : only synthesize the project"
	echo "-r  : only synthesize and place & root the project"
	echo "-sp : synthesize the project and program the fpga"
	echo "-c  : check syntax"
fi

cd ..
