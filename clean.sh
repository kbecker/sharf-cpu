#! /bin/sh  

cd asm
make clean
cd ..

rm vhdl/iram.dat vhdl/dram.dat cpu_tb cpu_tb.ghw work/*

find . -type f -name *~ | xargs rm -f

if [ -d "ise" ]; then
	find ise -type f -not -name *.prj -not -name *.lso -not -name *.xst | xargs rm -f

	find ise -type d -not -name xst -type d -not -name log -type d -not -name projnav.tmp -type d -not -name ise | xargs rm -rf

	rm *~ -rf
elif [ -d "xst" ]; then
	find ../ise -type f -not -name *.prj -not -name *.lso -not -name *.xst | xargs rm -f

	find ../ise -type d -not -name xst -type d -not -name log -type d -not -name projnav.tmp -type d -not -name ise | xargs rm -rf

	rm ../ise/*~ -rf
fi	
